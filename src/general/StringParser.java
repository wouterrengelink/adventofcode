package general;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParser {
	
	public static ArrayList<String> parseStringToCharacterList(String string) {
		ArrayList<String> characterList = new ArrayList<String>();
		
		for (char character : string.toCharArray()) {
			characterList.add("" + character);
		}
		
		return characterList;
	}
	
	public static ArrayList<String> splitOnSeparator(String inputString, String separator) {
		ArrayList<String> inputList = new ArrayList<String>();
		
		String[] splittedContent = inputString.split(separator);
		
		Collections.addAll(inputList, splittedContent);
		
		return inputList;
	}
	
	public static ArrayList<Integer> convertToIntegerList(ArrayList<String> stringList) {
		ArrayList<Integer> integerList = new ArrayList<Integer>();
		
		for (String string : stringList) {			
			int integer = Integer.parseInt(string);
			integerList.add(integer);
		}
		
		return integerList;
	}
	
	public static ArrayList<BigInteger> convertToBigIntegerList(ArrayList<String> stringList) {
		ArrayList<BigInteger> bigIntegerList = new ArrayList<BigInteger>();
		
		for (String string : stringList) {			
			BigInteger bigInteger = new BigInteger(string);
			bigIntegerList.add(bigInteger);
		}
		
		return bigIntegerList;
	}
	
	public static boolean hasDoubleChars(String input) {
		HashSet<Character> characterSet = new HashSet<Character>();
		char[] inputArray = input.toCharArray();
		
		for (int i = 0; i < input.length(); i++) {
			characterSet.add(inputArray[i]);
		}
		
		if (characterSet.size() < input.length()) {
			return true;
		}
		
		return false;
	}
	
	public static boolean containersWord(String fullString, String searchWord){
	    String regex = "\\b" + searchWord + "\\b";
	    Pattern pattern =Pattern.compile(regex);
	    Matcher matcher = pattern.matcher(fullString);
	    return matcher.find();
	}
	
}
