package general;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringParserTest {

	@Test
	void doubleValuesTest() {
		if (!StringParser.hasDoubleChars("1111")) {
			fail("Double values not recognized");
		}
		
		if (StringParser.hasDoubleChars("12345")) {
			fail("Double values not recognized");
		}
	}

}
