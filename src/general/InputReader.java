package general;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class InputReader {
	private String inputPath;
	
	public InputReader(String inputPath) {
		this.inputPath = inputPath;
	}
	
	public ArrayList<String> getInputFromCharacters() {
		ArrayList<String> characterList = new ArrayList<String>();
		String line = null;
		
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(this.inputPath));
			line = bufferedReader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (char character : line.toCharArray()) {
			characterList.add("" + character);
		}
		
		return characterList;
	}
	
	public ArrayList<String> getInputFromList() {
		ArrayList<String> inputList = new ArrayList<String>();
		
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(this.inputPath));
			
			String line = bufferedReader.readLine();
			
			while (line != null) {
				inputList.add(line);
				
				line = bufferedReader.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return inputList;
	}
	
	public ArrayList<String> getInputFromSeparator(String separator) {
		ArrayList<String> inputList = new ArrayList<String>();

		Scanner scanner;
		try {
			scanner = new Scanner(new File(this.inputPath)).useDelimiter(separator);
			String element = scanner.next();
			
			while(element != null) {
				inputList.add(this.sanitizeString(element));
				if (scanner.hasNext()) {
					element = scanner.next();
				} else {
					break;
				}
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
		return inputList;
	}
	
	private String sanitizeString(String string) {
		return string.replace("\n", "").replace("\r", "");
	}
	
}
