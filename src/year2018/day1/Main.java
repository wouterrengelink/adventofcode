package year2018.day1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import year2019.day1.Module;

public class Main {

	private static String inputFile = "./src/year2018/day1/input.txt";
	
	public static void main(String[] args) {
		int frequency = 0;
		HashMap<Integer, Boolean> frequencyMap = new HashMap<Integer, Boolean>();
		
		frequencyMap.put(new Integer(frequency), new Boolean(true));
		
		boolean doubleFrequencyFound = false;
		
		try {
			while (!doubleFrequencyFound) {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));
				
				String line = bufferedReader.readLine();
				
				while (line != null) {
					int difference = Integer.parseInt(line);
					
					frequency += difference;
					
					Integer mapFrequency = new Integer(frequency);
					
					if (frequencyMap.get(mapFrequency) != null) {
						System.out.println("First Frequency is " + mapFrequency);
						doubleFrequencyFound = true;
						break;
					} else {
						frequencyMap.put(mapFrequency, new Boolean(true));
					}
					
					line = bufferedReader.readLine();
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Final frequency is " + frequency);
	}

}
