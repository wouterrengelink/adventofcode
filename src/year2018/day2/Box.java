package year2018.day2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Box {
	private String id;
	private HashMap<Character, Integer> numberOfOccurencesMap;

	public Box(String id) {
		this.id = id;
	}
	
	public void calculateOccurences() {
		HashMap<Character, Integer> numberOfOccurencesMap = new HashMap<Character, Integer>();
		
		char[] characters = this.id.toCharArray();
		
		for (char character : characters) {
			Integer currentValue = numberOfOccurencesMap.get(new Character(character));
			
			if (currentValue == null) {
				currentValue = new Integer(0);
			}
			
			Integer occurence = currentValue + new Integer(1);
			
			numberOfOccurencesMap.put(new Character(character), occurence);
		}
		
		this.numberOfOccurencesMap = numberOfOccurencesMap;
	}
	
	public boolean hasCharacterTimes(int searchForAmountOfOccurences) {
		Iterator iterator = this.numberOfOccurencesMap.entrySet().iterator();
		
		while (iterator.hasNext()) {
			Map.Entry pair = (Map.Entry) iterator.next();
			
			Integer amountOfOccurences = (Integer) pair.getValue();
			
			if (amountOfOccurences.equals(new Integer(searchForAmountOfOccurences))) {
				return true;
			}
		}
		
		return false;
	}
	
}
