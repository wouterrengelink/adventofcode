package year2018.day2;

import java.util.ArrayList;

import general.InputReader;

public class Main {
	private static String inputFile = "./src/year2018/day2/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Day 2 2018");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> inputList = inputReader.getInputFromList();
		
		BoxFactory boxFactory = new BoxFactory(inputList);
		
		ArrayList<Box> boxList = boxFactory.getBoxes();
		
		for (Box box : boxList) {
			box.calculateOccurences();
		}
		
		int numberOfBoxesWithTwoCharacters = 0;
		for (Box box : boxList) {
			if (box.hasCharacterTimes(2)) {
				numberOfBoxesWithTwoCharacters++;
			}
		}
		
		int numberOfBoxesWithThreeCharacters = 0;
		for (Box box : boxList) {
			if (box.hasCharacterTimes(3)) {
				numberOfBoxesWithThreeCharacters++;
			}
		}
		
		int checksum = numberOfBoxesWithTwoCharacters * numberOfBoxesWithThreeCharacters;
		
		System.out.println("Checksum " + checksum);
	}

}
