package year2018.day2;

import java.util.ArrayList;

public class BoxFactory {
	private ArrayList<String> idList;
	
	public BoxFactory(ArrayList<String> idList) {
		this.idList = idList;
	}
	
	public ArrayList<Box> getBoxes() {
		ArrayList<Box> boxList = new ArrayList<Box>();
		
		for (String id : this.idList) {
			boxList.add(new Box(id));
		}
		
		return boxList;
	}
}
