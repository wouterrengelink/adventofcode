package year2019.day13;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
