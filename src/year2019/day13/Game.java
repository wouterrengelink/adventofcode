package year2019.day13;

import java.awt.Canvas;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;

import year2019.day11.ShiphullViewer;

public class Game {
	private Computer computer;
	private Board board;
	private int score;
	private Coordinate ballPosition;
	private Coordinate paddlePosition;
	private int joyStickValue;
	private BoardViewer boardViewer;
	
	public Game(ArrayList<BigInteger> program, int memorySize) {
		this.computer = new Computer(program, memorySize);
		this.board = new Board();
		this.boardViewer = new BoardViewer(this.board);
	}
	
	public void enableFreeMode() {
		this.computer.setValueAtIndex(0, new BigInteger("2"));
	}
	
	public boolean isHalted() {
		return this.computer.isHalted();
	}
	
	public void run() {		
		while(!this.computer.isHalted()) {
			int joyStickPosition = JoyStick.determineJoyStickInput(this.ballPosition, this.paddlePosition);

			BigInteger x = this.computer.run();
			BigInteger y = this.computer.run();
			BigInteger tileTypeInt = this.computer.run();
			
			if (x != null && y != null && tileTypeInt != null) {
				if (x.intValue() == -1 && y.intValue() == 0) {
					this.setScore(tileTypeInt.intValue());
					continue;
				}
				
				TileType tileType = null;
				
				switch (tileTypeInt.intValue()) {
					case 0:
						tileType = TileType.EMPTY;
						break;
					case 1:
						tileType = TileType.WALL;
						break;
					case 2:
						tileType = TileType.BLOCK;
						break;
					case 3:
						tileType = TileType.HORIZONTAL_PADDLE;
						this.paddlePosition = new Coordinate(x.intValue(), y.intValue());
						joyStickPosition = JoyStick.determineJoyStickInput(this.ballPosition, this.paddlePosition);
						break;
					case 4:
						tileType = TileType.BALL;
						this.ballPosition = new Coordinate(x.intValue(), y.intValue());
						joyStickPosition = JoyStick.determineJoyStickInput(this.ballPosition, this.paddlePosition);
						break;
				}
				
				this.board.addTile(new Coordinate(x.intValue(), y.intValue()), new Tile(tileType));
				this.computer.setInput(new BigInteger("" + joyStickPosition));
				
				if (tileTypeInt.intValue() == 3 || tileTypeInt.intValue() == 4) {
//					this.boardViewer.show();
					System.out.println("Score: " + this.score);
//					try {
//						Thread.sleep(10);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
				}
			}
		}
		
		
	}
	
	private void setScore(int score) {
		this.score = score;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public Board getBoard() {
		return this.board;
	}
	
}
