package year2019.day13;

public enum TileType {
	EMPTY,
	WALL,
	BLOCK,
	HORIZONTAL_PADDLE,
	BALL
}
