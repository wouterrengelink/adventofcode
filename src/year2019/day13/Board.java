package year2019.day13;

import java.util.HashMap;

public class Board {
	private HashMap<Coordinate, Tile> board;
	
	public Board() {
		this.board = new HashMap<Coordinate, Tile>();
	}
	
	public void addTile(Coordinate coordinate, Tile tile) {
		this.board.put(coordinate, tile);
	}
	
	public HashMap<Coordinate, Tile> getBoard() {
		return this.board;
	}
	
	public Coordinate getBallPosition() {
		for (HashMap.Entry<Coordinate, Tile> entry : this.board.entrySet()) {
			Tile tile = entry.getValue();
			
			if (tile.getType() == TileType.BALL) {
				return entry.getKey();
			}
		}
		
		return null;
	}
	
	public Coordinate getPaddlePosition() {
		for (HashMap.Entry<Coordinate, Tile> entry : this.board.entrySet()) {
			Tile tile = entry.getValue();
			
			if (tile.getType() == TileType.HORIZONTAL_PADDLE) {
				return entry.getKey();
			}
		}
		
		return null;
	}
	
	public int size() {
		return this.board.size();
	}
}
