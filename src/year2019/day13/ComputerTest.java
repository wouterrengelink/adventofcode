package year2019.day13;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import general.InputReader;
import general.StringParser;

class ComputerTest {

	@Test
	void positionModeTest() {
		String programString = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9";
				
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));

		Computer computer = new Computer(program, 2048);
		
		System.out.println("Position output");
		computer.setInput(new BigInteger("0"));
		
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("1"));
		
		if (!computer.run().equals(new BigInteger("1"))) {
			fail("Output not 1");
		}
	}
	
	@Test
	void immediateModeTest() {
		String programString = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Immediate output");
		computer.setInput(new BigInteger("0"));
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("1"));
		if (!computer.run().equals(new BigInteger("1"))) {
			fail("Output not 1");
		}
		
	}

	@Test
	void largerTest() {
		InputReader inputReader = new InputReader("./src/year2019/day7/largerTest.txt");
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("larger output");
		computer.setInput(new BigInteger("7"));
		if (!computer.run().equals(new BigInteger("999"))) {
			fail("Output not 999");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("8"));
		if (!computer.run().equals(new BigInteger("1000"))) {
			fail("Output not 1000");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("9"));
		if (!computer.run().equals(new BigInteger("1001"))) {
			fail("Output not 1001");
		}
		
	}
	
	@Test
	void positionMode8Equal() {
		String programString = "3,9,8,9,10,9,4,9,99,-1,8";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Position 8 equals output");
		computer.setInput(new BigInteger("8"));
		
		BigInteger output = computer.run();
		
		if (!output.equals(new BigInteger("1"))) {
			fail("Output not one");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("1"));
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
	}
	
	@Test
	void positionMode8less() {
		String programString = "3,9,7,9,10,9,4,9,99,-1,8";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Position 8 less output");
		computer.setInput(new BigInteger("7"));
		if (!computer.run().equals(new BigInteger("1"))) {
			fail("Output not one");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("8"));
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
	}
	
	@Test
	void immidiateMode8Equal() {
		String programString = "3,3,1108,-1,8,3,4,3,99";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Immidiate 8 equals output");
		computer.setInput(new BigInteger("8"));
		if (!computer.run().equals(new BigInteger("1"))) {
			fail("Output not one");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("1"));
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
	}
	
	@Test
	void immidiateMode8less() {
		String programString = "3,3,1107,-1,8,3,4,3,99";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Immediate 8 less output");
		computer.setInput(new BigInteger("7"));
		if (!computer.run().equals(new BigInteger("1"))) {
			fail("Output not one");
		}
		
		computer.reset();
		computer.setInput(new BigInteger("8"));
		if (!computer.run().equals(new BigInteger("0"))) {
			fail("Output not zero");
		}
		
	}
	
	@Test
	void copyOfSelfTest() {
		String programString = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
		String outputString = "";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("copyOfSelfTest relative output");
		
		while(!computer.isHalted()) {
			BigInteger output = computer.run();
			
			if (output != null) {
				outputString += output + ",";
			}
		}
		
		String adjustedProgramString = programString + ",";
		if (!adjustedProgramString.equals(outputString)) {
			fail("Output is not program");
		}
	}
	
	@Test
	void Digits16Number() {
		String programString = "1102,34915192,34915192,7,4,7,99,0";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("Digits16Number relative output");
		
		BigInteger output = computer.run();
		String outputString = "" + output;
		
		if (outputString.length() != 16) {
			fail("Output not 16 characters");
		}
		
	}
	
	@Test
	void outputLargeNumber() {
		String programString = "104,1125899906842624,99";
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(StringParser.splitOnSeparator(programString, ","));
		
		Computer computer = new Computer(program, 2048);
		
		System.out.println("outputLargeNumber relative output");
		
		BigInteger output = computer.run();
		
		if (!output.equals(new BigInteger("1125899906842624"))) {
			fail("Output not 1125899906842624");
		}
		
	}
	
	
}
