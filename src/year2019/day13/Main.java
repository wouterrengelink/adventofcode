package year2019.day13;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day13/input.txt";
	private static int memorySize = 4096;
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Day 13 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		JoyStick joyStick = new JoyStick();
		
		Game game = new Game(program, memorySize);
		
		game.enableFreeMode();

		game.run();			

		System.out.println("Score: " + game.getScore());
		
		System.out.println("Done");
	}

}
