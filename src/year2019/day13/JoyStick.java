package year2019.day13;

public class JoyStick {
	
	public static int determineJoyStickInput(Coordinate ballCoordinate, Coordinate paddleCoordinate) {
		if (ballCoordinate == null || paddleCoordinate == null) {
			return 0;
		}
		
		if(ballCoordinate.getX() < paddleCoordinate.getX()) {
			return -1;
		} else if (ballCoordinate.getX() > paddleCoordinate.getX()) {
			return 1;
		} else {
			return 0;
		}
	}
}
