package year2019.day13;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;

import javax.swing.JFrame;

public class BoardViewer extends Canvas {
	private Board board;
	
	public BoardViewer(Board board) {
		this.board = board;
	}

	public void show() {
		JFrame frame = new JFrame("GameBoard");
        Canvas canvas = new BoardViewer(this.board);
        canvas.setSize(900, 500);
        canvas.setBackground(java.awt.Color.BLACK);
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}
	
	public void paint(Graphics g) {
		for (HashMap.Entry<Coordinate, Tile> entry : this.board.getBoard().entrySet()) {
			Tile tile = entry.getValue();
			Coordinate coordinate = entry.getKey();
			Color tileColor = null;
			
			switch (tile.getType()) {
				case EMPTY:
					tileColor = Color.BLACK;
					break;
				case BLOCK:
					tileColor = Color.RED;
					break;
				case WALL:
					tileColor = Color.BLUE;
					break;
				case HORIZONTAL_PADDLE:
					tileColor = Color.GREEN;
					break;
				case BALL:
					tileColor = Color.PINK;
					break;
			}
			
			g.setColor(tileColor);
			g.fillRect(coordinate.getX() * 20, coordinate.getY() * 20, 20, 20);
		}		
    }
	
	public int getNumberOfTiles(TileType tileType) {
		int numberOfTiles = 0;
		
		for (HashMap.Entry<Coordinate, Tile> entry : this.board.getBoard().entrySet()) {
			Tile tile = entry.getValue();
			
			if (tile.getType() == tileType) {
				numberOfTiles++;
			}
		}
		
		return numberOfTiles;
	}
	
}
