package year2019.day7;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day7/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Day 7 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		BigInteger maxThrusterOutput = new BigInteger("-1");
		
		String phaseSettings = "";
		
		for (int i = 5; i <= 9; i++)
		for (int j = 5; j <= 9; j++)
		for (int k = 5; k <= 9; k++)
		for (int l = 5; l <= 9; l++)
		for (int m = 5; m <= 9; m++) {
			if (StringParser.hasDoubleChars("" + i + j + k + l + m)) {
				continue;
			}
			
			AmplifierBattery amplifierBattery = new AmplifierBattery(program, i, j, k, l, m);
			
			BigInteger thrusterOutput = amplifierBattery.run(new BigInteger("0"));
			
			if (thrusterOutput.compareTo(maxThrusterOutput) == 1) {
				phaseSettings = "" + i + j + k + l + m;
				maxThrusterOutput = thrusterOutput;
			}
		}
		
		System.out.println("Thruster output " + maxThrusterOutput);
		System.out.println("From phase " + phaseSettings);
		
		System.out.println("Done");
	}

}
