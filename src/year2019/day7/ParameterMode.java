package year2019.day7;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
