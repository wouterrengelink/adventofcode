package year2019.day7;

import java.math.BigInteger;
import java.util.ArrayList;

public class AmplifierBattery {
	private Amplifier amplifier1;
	private Amplifier amplifier2;
	private Amplifier amplifier3;
	private Amplifier amplifier4;
	private Amplifier amplifier5;
	private BigInteger phase1;
	private BigInteger phase2;
	private BigInteger phase3;
	private BigInteger phase4;
	private BigInteger phase5;
	
	public AmplifierBattery(ArrayList<BigInteger> program, int phase1, int phase2, int phase3, int phase4, int phase5) {
		this.amplifier1 = new Amplifier(program);
		this.amplifier2 = new Amplifier(program);
		this.amplifier3 = new Amplifier(program);
		this.amplifier4 = new Amplifier(program);
		this.amplifier5 = new Amplifier(program);
		this.phase1 = new BigInteger("" + phase1);
		this.phase2 = new BigInteger("" + phase2);
		this.phase3 = new BigInteger("" + phase3);
		this.phase4 = new BigInteger("" + phase4);
		this.phase5 = new BigInteger("" + phase5);
	}
	
	public BigInteger run(BigInteger input) {
		BigInteger amplifier1output = this.amplifier1.run(this.phase1, input);
		BigInteger amplifier2output = this.amplifier2.run(this.phase2, amplifier1output);
		BigInteger amplifier3output = this.amplifier3.run(this.phase3, amplifier2output);
		BigInteger amplifier4output = this.amplifier4.run(this.phase4, amplifier3output);
		BigInteger amplifier5output = this.amplifier5.run(this.phase5, amplifier4output);
		
		while(!this.amplifier5.isHalted()) {
			amplifier1output = this.amplifier1.run(amplifier5output);
			amplifier2output = this.amplifier2.run(amplifier1output);
			amplifier3output = this.amplifier3.run(amplifier2output);
			amplifier4output = this.amplifier4.run(amplifier3output);
			amplifier5output = this.amplifier5.run(amplifier4output);

			if (amplifier5output != null) {
				input = amplifier5output;
			}
		}
		
		return input;
	}
	
}
