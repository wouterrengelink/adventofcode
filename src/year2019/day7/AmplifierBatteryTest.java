package year2019.day7;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import general.InputReader;
import general.StringParser;

class AmplifierBatteryTest {
	
	@Test
	void test139629729() {
		String inputFile = "./src/year2019/day7/test139629729.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		AmplifierBattery amplifierBattery = new AmplifierBattery(program, 9, 8, 7, 6, 5);
		
		BigInteger thrusterOutput = amplifierBattery.run(new BigInteger("0"));
		
		if (!thrusterOutput.equals(new BigInteger("139629729"))) {
			System.out.println("Test 139629729 output " + thrusterOutput);
			fail("Thruster output not 139629729");
		}
	}
	
	@Test
	void test18216() {
		String inputFile = "./src/year2019/day7/test18216.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		AmplifierBattery amplifierBattery = new AmplifierBattery(program, 9, 7, 8, 5, 6);
		
		BigInteger thrusterOutput = amplifierBattery.run(new BigInteger("0"));
		
		if (!thrusterOutput.equals(new BigInteger("18216"))) {
			System.out.println("Test 18216 output " + thrusterOutput);
			fail("Thruster output not 18216");
		}
	}

}
