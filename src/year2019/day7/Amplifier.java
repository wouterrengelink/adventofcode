package year2019.day7;

import java.math.BigInteger;
import java.util.ArrayList;

public class Amplifier {
	private Computer computer;
	private boolean hasPhaseSetting;
	private static int memorySize = 4096;
	
	public Amplifier(ArrayList<BigInteger> program) {
		this.computer = new Computer(program, memorySize);
		this.hasPhaseSetting = false;
	}
	
	public BigInteger run(BigInteger phase, BigInteger input) {
		this.computer.addInput(phase);
		this.computer.addInput(input);
		
		return this.computer.run(); 
	}
	
	public BigInteger run(BigInteger input) {
		this.computer.addInput(input);
		
		return this.computer.run(); 
	}
	
	public boolean isHalted() {
		return this.computer.isHalted();
	}
	
}
