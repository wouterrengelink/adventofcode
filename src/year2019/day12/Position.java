package year2019.day12;

public class Position {
	int x;
	int y;
	int z;
	
	public Position(Position position) {
		this.x = position.getX();
		this.y = position.getY();
		this.z = position.getZ();
	}
	
	public Position(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getZ() {
		return this.z;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	@Override
	public boolean equals(Object object) { 
		if (object == this) { 
		    return true; 
		} 
	  
	    if (!(object instanceof Position)) { 
	        return false; 
	    } 
	      
	    Position position = (Position) object; 
	      
	    if (this.x == position.getX() && this.y == position.getY() && this.z == position.getZ()) {
	    	return true;
	    }
	    
	    return false;
	} 
	
}
