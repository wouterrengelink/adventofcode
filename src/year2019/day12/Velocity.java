package year2019.day12;

public class Velocity {
	private int x;
	private int y;
	private int z;
	
	public Velocity(Velocity velocity) {
		this.x = velocity.x;
		this.y = velocity.y;
		this.z = velocity.z;
	}
	
	public Velocity(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setZ(int z) {
		this.z = z;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getZ() {
		return this.z;
	}
	
	@Override
	public boolean equals(Object object) { 
		if (object == this) { 
		    return true; 
		} 
	  
	    if (!(object instanceof Velocity)) { 
	        return false; 
	    } 
	      
	    Velocity velocity = (Velocity) object; 
	      
	    if (this.x == velocity.getX() && this.y == velocity.getY() && this.z == velocity.getZ()) {
	    	return true;
	    }
	    
	    return false;
	} 
	
}
