package year2019.day12;

import java.util.ArrayList;

import year2019.day10.Coordinate;

public class Moon {
	private Position position;
	private Velocity velocity;
	
	public Moon(Moon moon) {
		this.position = new Position(moon.getPosition());
		this.velocity = new Velocity(moon.getVelocity());
	}
	
	public Moon(int x, int y, int z) {
		this.position = new Position(x, y, z);
		this.velocity = new Velocity(0, 0, 0);
	}
	
	public void applyGravity(ArrayList<Moon> moonList) {
		for (Moon moon : moonList) {
			if (moon == this) {
				continue;
			}
			
			if (this.position.getX() > moon.position.getX()) {
				this.velocity.setX(this.velocity.getX() - 1);
			} else if (this.position.getX() < moon.position.getX()) {
				this.velocity.setX(this.velocity.getX() + 1);
			}
			
			if (this.position.getY() > moon.position.getY()) {
				this.velocity.setY(this.velocity.getY() - 1);
			} else if (this.position.getY() < moon.position.getY()) {
				this.velocity.setY(this.velocity.getY() + 1);
			}
			
			if (this.position.getZ() > moon.position.getZ()) {
				this.velocity.setZ(this.velocity.getZ() - 1);
			} else if (this.position.getZ() < moon.position.getZ()) {
				this.velocity.setZ(this.velocity.getZ() + 1);
			}
		}
	}
	
	public void applyVelocity() {
		this.position.setX(this.position.getX() + this.velocity.getX());
		this.position.setY(this.position.getY() + this.velocity.getY());
		this.position.setZ(this.position.getZ() + this.velocity.getZ());
	}
	
	private int getPotentialEnergy() {
		int absX = Math.abs(this.position.getX());
		int absY = Math.abs(this.position.getY());
		int absZ = Math.abs(this.position.getZ());
		
		return absX + absY + absZ;
	}
	
	private int getKineticEnergy() {
		int absX = Math.abs(this.velocity.getX());
		int absY = Math.abs(this.velocity.getY());
		int absZ = Math.abs(this.velocity.getZ());
		
		return absX + absY + absZ;
	}
	
	public int getTotalEnergy() {
		return this.getPotentialEnergy() * this.getKineticEnergy();
	}
	
	public Velocity getVelocity() {
		return this.velocity;
	}
	
	public Position getPosition() {
		return this.position;
	}
	
	@Override
	public boolean equals(Object object) { 
		if (object == this) { 
		    return true; 
		} 
	  
	    if (!(object instanceof Moon)) { 
	        return false; 
	    } 
	      
	    Moon moon = (Moon) object; 
	      
	    if (this.position.equals(moon.getPosition()) && this.velocity.equals(moon.getVelocity())) {
	    	return true;
	    }
	    
	    return false;
	} 
	
}
