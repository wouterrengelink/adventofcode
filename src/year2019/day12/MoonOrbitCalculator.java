package year2019.day12;

import java.math.BigInteger;
import java.util.ArrayList;

public class MoonOrbitCalculator {
	private int numberOfSteps;
	private ArrayList<Moon> moonList;
	
	private ArrayList<Integer> xSteps;
	private ArrayList<Integer> ySteps;
	private ArrayList<Integer> zSteps;
	
	public MoonOrbitCalculator(ArrayList<Moon> moonList, int numberOfSteps) {
		this.numberOfSteps = numberOfSteps;
		this.moonList = moonList;
		
		this.xSteps = new ArrayList<Integer>();
		this.ySteps = new ArrayList<Integer>();
		this.zSteps = new ArrayList<Integer>();
	}
	
	public int calculateTotalEnergy() {
		ArrayList<Moon> modifyableMoonList = new ArrayList<Moon>(this.moonList);
		for (int i = 0; i < this.numberOfSteps; i++) {
			for (Moon moon : modifyableMoonList) {
				moon.applyGravity(modifyableMoonList);
			}

			for (Moon moon : modifyableMoonList) {
				moon.applyVelocity();
			}
		}

		int totalEnergyInSystem = 0;
		for (Moon moon : modifyableMoonList) {
			totalEnergyInSystem += moon.getTotalEnergy();
		}
		
		return totalEnergyInSystem;
	}
	
	public void calculateNumberOfStepsBeforeItsBackToOriginalPosition(int index) {
		ArrayList<Moon> modifyableMoonList = new ArrayList<Moon>();
		
		for (Moon moon : this.moonList) {
			modifyableMoonList.add(new Moon(moon));
		}
		
		Position initialPosition = new Position(this.moonList.get(index).getPosition());
		Velocity initialVelocity = new Velocity(this.moonList.get(index).getVelocity());
		
		BigInteger numberOfSteps = new BigInteger("0");
		boolean positionFound = false;
		
		int xStep = -1;
		int yStep = -1;
		int zStep = -1;
		
		while(!positionFound) {
			
			for (Moon moon : modifyableMoonList) {
				moon.applyGravity(modifyableMoonList);
			}

			for (Moon moon : modifyableMoonList) {
				moon.applyVelocity();
			}
			
			numberOfSteps = numberOfSteps.add(new BigInteger("1"));
			
			Moon checkMoon = modifyableMoonList.get(index);

			if (xStep == -1) {
				this.xSteps.add(new Integer(checkMoon.getPosition().getX()));
			}
			
			if (yStep == -1) {
				this.ySteps.add(new Integer(checkMoon.getPosition().getY()));
			}
			
			if (zStep == -1) {
				this.zSteps.add(new Integer(checkMoon.getPosition().getZ()));
			}
			
			if (xStep == -1 && checkMoon.getPosition().getX() != initialPosition.getX()) {
				xStep = numberOfSteps.intValue();
				
				System.out.println("X the same on " + numberOfSteps);
			}
			
			if (yStep == -1 && checkMoon.getPosition().getY() == initialPosition.getY()) {
				yStep = numberOfSteps.intValue();
				
				this.ySteps.add(checkMoon.getPosition().getY());
				
				System.out.println("Y the same on " + numberOfSteps);
			}
			
			if (zStep == -1 && checkMoon.getPosition().getZ() == initialPosition.getZ()) {
				zStep = numberOfSteps.intValue();
				
				System.out.println("Z the same on " + numberOfSteps);
			}
			
			if (xStep != -1 && yStep != -1 && zStep != -1) {
				//return new BigInteger("" + (xStep * yStep * zStep));
				System.out.println("All orbits found");
				break;
			}
		}

	}
	
	public int getXValueOnPosition(int index) {
		int testIndex = index % this.xSteps.size();
		
		return this.xSteps.get(testIndex);
	}
	
	public int getYValueOnPosition(int index) {
		int testIndex = index % this.ySteps.size();
		
		return this.ySteps.get(testIndex);
	}
	
	public int getZValueOnPosition(int index) {
		int testIndex = index % this.zSteps.size();
		
		return this.zSteps.get(testIndex);
	}
	
	// if (numberOfSteps % stepX == 0
	
}
