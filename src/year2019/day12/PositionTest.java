package year2019.day12;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PositionTest {

	@Test
	void test() {
		Position firstPosition = new Position(1, -10, 8);
		Position secondPosition = new Position(1, -10, 8);
		
		if (!firstPosition.equals(secondPosition)) {
			fail("Positions not equals");
		}
		
		Position firstNotEqualPosition = new Position(1, -10, 8);
		Position secondNotEqualPosition = new Position(-1, 8, 5);
		
		if (firstNotEqualPosition.equals(secondNotEqualPosition)) {
			fail("Positions should not be equal");
		}
	}

}
