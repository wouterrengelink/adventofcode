package year2019.day12;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;

public class Main {
	private static String inputFile = "./src/year2019/day12/input.txt";
	private static int numberOfSteps = 1000;
	
	public static void main(String[] args) {
		System.out.println("Day 12 year 2019");

		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> input = inputReader.getInputFromList();
		
		ArrayList<Moon> moonList = MoonParser.parseInput(input);
		
		MoonOrbitCalculator moonOrbitCalculator = new MoonOrbitCalculator(moonList, numberOfSteps);
		
//		BigInteger moonASteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(0);
//		System.out.println("A steps: " + moonASteps);
//		BigInteger moonBSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(1);
//		System.out.println("B steps: " + moonBSteps);
//		BigInteger moonCSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(2);
//		System.out.println("C steps: " + moonCSteps);
//		BigInteger moonDSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(3);
//		System.out.println("A steps: " + moonDSteps);
//	
//		System.out.println("Done");
	}

}
