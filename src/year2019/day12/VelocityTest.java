package year2019.day12;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class VelocityTest {

	@Test
	void test() {
		Velocity firstVelocity = new Velocity(1, -10, 8);
		Velocity secondVelocity = new Velocity(1, -10, 8);
		
		if (!firstVelocity.equals(secondVelocity)) {
			fail("Velocities not equals");
		}
		
		Velocity firstNotEqualVelocity = new Velocity(1, -10, 8);
		Velocity secondNotEqualVelocity = new Velocity(-1, 8, 5);
		
		if (firstNotEqualVelocity.equals(secondNotEqualVelocity)) {
			fail("Velocities should not be equal");
		}
	}

}
