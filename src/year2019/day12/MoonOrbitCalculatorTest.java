package year2019.day12;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import general.InputReader;

class MoonOrbitCalculatorTest {

	@Test
	void firstExampleTest() {
		String inputFile = "./src/year2019/day12/firstTest.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> input = inputReader.getInputFromList();
		
		ArrayList<Moon> moonList = MoonParser.parseInput(input);
		
//		<x=-1, y=0, z=2>
//		<x=2, y=-10, z=-7>
//		<x=4, y=-8, z=8>
//		<x=3, y=5, z=-1>
		
		MoonOrbitCalculator moonOrbitCalculator = new MoonOrbitCalculator(moonList, 10);

		if (moonOrbitCalculator.calculateTotalEnergy() != 179) {
			System.out.println(moonOrbitCalculator.calculateTotalEnergy());
			fail("Total energy not 179");	
		}
		
		moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(0);
		

		int xPositionAtIndex = moonOrbitCalculator.getXValueOnPosition(2772);
		int yPositionAtIndex = moonOrbitCalculator.getYValueOnPosition(2772);
		int zPositionAtIndex = moonOrbitCalculator.getZValueOnPosition(2772);

		System.out.println("" + xPositionAtIndex);
		System.out.println("" + yPositionAtIndex);
		System.out.println("" + zPositionAtIndex);		
	}
	
	@Test
	void secondExampleTest() {
		String inputFile = "./src/year2019/day12/secondTest.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> input = inputReader.getInputFromList();
		
		ArrayList<Moon> moonList = MoonParser.parseInput(input);
		
//		<x=-8, y=-10, z=0>
//		<x=5, y=5, z=10>
//		<x=2, y=-7, z=3>
//		<x=9, y=-8, z=-3>
				
		MoonOrbitCalculator moonOrbitCalculator = new MoonOrbitCalculator(moonList, 100);

		if (moonOrbitCalculator.calculateTotalEnergy() != 1940) {
			System.out.println(moonOrbitCalculator.calculateTotalEnergy());
			fail("Total energy not 1940");	
		}
		
//		if (!moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(0).equals(moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(0))) {
//			fail("Values should be the same: 0");
//		}
//		
//		if (!moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(1).equals(moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(1))) {
//			fail("Values should be the same: 1");
//		}
//		
//		if (!moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(2).equals(moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(2))) {
//			fail("Values should be the same: 2");
//		}
//		
//		if (!moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(3).equals(moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(3))) {
//			fail("Values should be the same: 3");
//		}
		
//		System.out.println("Searching for equals of position 0");
//		BigInteger moonASteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(0);
//		System.out.println("Steps " + moonASteps);
		
//		BigInteger moonBSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(1);
//		System.out.println("Steps " + moonBSteps);
//		
//		BigInteger moonCSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(2);
//		System.out.println("Steps " + moonCSteps);
//		
//		BigInteger moonDSteps = moonOrbitCalculator.calculateNumberOfStepsBeforeItsBackToOriginalPosition(3);
//		System.out.println("Steps " + moonDSteps);
		System.out.println("Done");
	}

}
