package year2019.day12;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoonParser {
	
	public static ArrayList<Moon> parseInput(ArrayList<String> input) {
		ArrayList<Moon> moonList = new ArrayList<Moon>();
		
		String patternString = "\\<x=(-?[0-9]+), y=(-?[0-9]+), z=(-?[0-9]+)\\>";
		Pattern pattern = Pattern.compile(patternString);
		
		for (String moonCoordinate : input) {
			Matcher matcher = pattern.matcher(moonCoordinate);
			
			matcher.find();
			
			int x = Integer.parseInt(matcher.group(1));
			int y = Integer.parseInt(matcher.group(2));
			int z = Integer.parseInt(matcher.group(3));
			
			moonList.add(new Moon(x, y, z));
		}
		
		return moonList;
	}
}
