package year2019.day8;

import general.InputReader;

public class Main {
	private static String inputFile = "./src/year2019/day8/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Initializing day 8 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		
		String imageData = inputReader.getInputFromList().get(0);
		
		Image image = new Image(imageData, 25, 6);

		Layer fewestLayer = image.getLayerWithFewestDigits("0");
		
		int oneDigitCount = fewestLayer.getDigitCount("1");
		int twoDigitCount = fewestLayer.getDigitCount("2");
		
		int answer = oneDigitCount * twoDigitCount;
		
		System.out.println("Answer: " + answer);
		
		image.renderImage();
		
		System.out.println("Done");
	}

}
