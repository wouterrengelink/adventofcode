package year2019.day8;

import java.util.ArrayList;

public class Image {
	private int width;
	private int height;
	private ArrayList<Layer> layers;
	
	public Image(String imageData, int width, int height) {
		this.width = width;
		this.height = height;
		this.layers = new ArrayList<Layer>();
		
		this.parseImageData(imageData);
	}
	
	private void parseImageData(String imageData) {
		int pixelsPerLayer = this.width * this.height;
		
		int layerCount = imageData.length() / pixelsPerLayer;
		
		int startIndex = 0;
		
		for (int i = 0; i < layerCount; i++) {
			String layerData = imageData.substring(startIndex, startIndex + pixelsPerLayer);
			
			Layer layer = new Layer(layerData, this.width, this.height);
			
			this.layers.add(layer);
			
			startIndex += pixelsPerLayer;
		}
	}
	
	public Layer getLayerWithFewestDigits(String digit) {
		Layer fewestLayer = null;
		int fewestDigits = 99999999;
		
		for (Layer layer : this.layers) {
			int digitCount = layer.getDigitCount(digit);
			
			if (digitCount < fewestDigits) {
				fewestLayer = layer;
				fewestDigits = digitCount;
			}
		}
		
		return fewestLayer;
	}
	
	public void renderImage() {
		StringBuilder finalImage = this.getEmptyImage();
		
		for (int i = this.layers.size() - 1; i >= 0; i--) {
			Layer layer = this.layers.get(i);
			this.parseLayerData(finalImage, layer);
			
		}
		
		this.outputImage(finalImage);
	}
	
	private void outputImage(StringBuilder image) {
		int startIndex = 0;
		
		for(int i = 0; i < this.height; i++) {
			String line = image.substring(startIndex, startIndex + this.width);
			System.out.println(line);
			startIndex += this.width;
		}

	}
	
	private void parseLayerData(StringBuilder result, Layer layer) {
		String layerData = layer.getLayerData();
		
		for (int i = 0; i < layerData.length(); i++) {
			char pixel = layerData.charAt(i);
			
			switch (pixel) {
				case '0':
				case '1':
					result.setCharAt(i, pixel);
					break;
				case '2':
					break;
			}
		}
	}
	
	private StringBuilder getEmptyImage() {
		int pixelCount = this.width * this.height;
		String zeroedString = "";
		
		for (int i = 0; i < pixelCount; i++) {
			zeroedString += "0";
		}
		
		StringBuilder emptyImage = new StringBuilder(zeroedString);
		
		return emptyImage;
	}
	
}
