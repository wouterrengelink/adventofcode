package year2019.day8;

public class Layer {
	private int width;
	private int height;
	private String layerData;
	
	public Layer(String layerData, int width, int height) {
		this.width = width;
		this.height = height;
		this.layerData = layerData;
	}
	
	public int getDigitCount(String digit) {
		int digitCount = 0;
		
		for (int i = 0; i < this.layerData.length(); i++) {
			String testCharacter = "" + this.layerData.charAt(i);
			
			if (testCharacter.equals(digit)) {
				digitCount++;
			}
		}
		
		return digitCount;
	}
	
	public String getLayerData() {
		return this.layerData;
	}
	
}
