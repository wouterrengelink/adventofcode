package year2019.day9;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
