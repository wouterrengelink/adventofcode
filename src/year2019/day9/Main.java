package year2019.day9;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day9/input.txt";
	
	public static void main(String[] args) {
		InputReader inputReader = new InputReader(inputFile);

		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		Computer computer = new Computer(program, 12048);
		
		computer.addInput(new BigInteger("2"));
		
		while (!computer.isHalted()) {
			BigInteger output = computer.run();
			
			System.out.println("Output " + output);
		}
		
	}

}
