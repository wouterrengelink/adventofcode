package year2019.day9;

public class InstructionParser {
	
	public static Instruction parse(int instruction) {
		String instructionString = "" + instruction;
		
		switch (instructionString.length()) {
			case 1:
			case 2:
				return new Instruction(instruction, ParameterMode.POSITION, ParameterMode.POSITION, ParameterMode.POSITION);
			case 3:
				return InstructionParser.parseThreeCharacters(instructionString);
			case 4:
				return InstructionParser.parseFourCharacters(instructionString);
			case 5:
				return InstructionParser.parseFiveCharacters(instructionString);
		}
		
		return null;
	}
	
	private static Instruction parseThreeCharacters(String instruction) {
		int opCode = InstructionParser.getOpCode(instruction);
		int firstParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 3));
		
		return new Instruction(opCode, ParameterModeParser.parseParameterMode(firstParameterModeInteger), ParameterMode.POSITION, ParameterMode.POSITION);
	}
	
	private static Instruction parseFourCharacters(String instruction) {
		int opCode = InstructionParser.getOpCode(instruction);
		int firstParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 3));
		int secondParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 4));
		
		return new Instruction(opCode, ParameterModeParser.parseParameterMode(firstParameterModeInteger), ParameterModeParser.parseParameterMode(secondParameterModeInteger), ParameterMode.POSITION);
	}
	
	private static Instruction parseFiveCharacters(String instruction) {
		int opCode = InstructionParser.getOpCode(instruction);
		int firstParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 3));
		int secondParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 4));
		int thirdParameterModeInteger = Integer.parseInt("" + instruction.charAt(instruction.length() - 5));
		
		return new Instruction(opCode, ParameterModeParser.parseParameterMode(firstParameterModeInteger), ParameterModeParser.parseParameterMode(secondParameterModeInteger), ParameterModeParser.parseParameterMode(thirdParameterModeInteger));
	}
	
	private static int getOpCode(String instruction) {
		String subString = instruction.substring(instruction.length() - 2, instruction.length());
		
		return Integer.parseInt(subString);
	}
	
}
