package year2019.day1;

public class Module {
	private int mass;
	
	public Module(int mass) {
		this.mass = mass;
	}
	
	private static int calculateFuel(int mass) {
		double dividedMass = (double) mass / 3.0;
		
		int roundedMass = (int) dividedMass;
		
		int subtractedMass = roundedMass - 2;
		
		return subtractedMass;
	}
	
	public int calculateFuel() {
		int totalFuel = Module.calculateFuel(this.mass);
		
		int additionalFuel = Module.calculateFuel(totalFuel);
		
		while (additionalFuel > 0) {
			totalFuel += additionalFuel;
			
			int newFuel = Module.calculateFuel(additionalFuel);
			
			additionalFuel = newFuel;
		}
		
		return totalFuel;
	}
	
}
