package year2019.day1;

import java.util.ArrayList;

public class Part1 {
	private static String inputFile = "./src/year2019/day1/input.txt";
			
	public static void main(String[] args) {
		ModuleParser moduleParser = new ModuleParser(inputFile);
		
		ArrayList<Module> moduleList = moduleParser.getModules();
		
		int totalFuel = 0;
		
		for (Module module : moduleList) {
			totalFuel += module.calculateFuel();
		}
		
		System.out.println("The total fuel is " + totalFuel);
	}

}
