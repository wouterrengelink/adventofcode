package year2019.day1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ModuleParser {
	private String inputFile;
	
	public ModuleParser(String inputFile) {
		this.inputFile = inputFile;
	}
	
	public ArrayList<Module> getModules() {
		ArrayList<Module> moduleList = new ArrayList<Module>();
		
		BufferedReader bufferedReader;
		
		try {
			bufferedReader = new BufferedReader(new FileReader(this.inputFile));
			
			String line = bufferedReader.readLine();
			
			while (line != null) {
				Module module = new Module(Integer.parseInt(line));
				moduleList.add(module);
				line = bufferedReader.readLine();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return moduleList;
	}
}
