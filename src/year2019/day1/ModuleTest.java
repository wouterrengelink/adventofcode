package year2019.day1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ModuleTest {

//	@Test
//	void test12() {
//		Module module = new Module(12);
//		
//		if (module.calculateFuel() != 2) {
//			fail("mass 12 test failed");
//		}
//	}

	@Test
	void test14() {
		Module module = new Module(14);
		
		if (module.calculateFuel() != 2) {
			fail("mass 14 test failed");
		}
	}
	
	@Test
	void test1969() {
		Module module = new Module(1969);
		
		if (module.calculateFuel() != 966) {
			fail("mass 1969 test failed");
		}
	}
	
	@Test
	void test100756() {
		Module module = new Module(100756);
		
		if (module.calculateFuel() != 50346) {
			fail("mass 100756 test failed");
		}
	}
	
}
