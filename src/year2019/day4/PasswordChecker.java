package year2019.day4;

public class PasswordChecker {
	private String password;
	
	public PasswordChecker(String password) {
		this.password = password;
	}
	
	public boolean isValidPassword() {
		if (!this.hasSixDigits()) {
			return false;
		}
		
		if (!this.hasIncreasingDigits()) {
			return false;
		}
		
		if (!this.hasSameDigits()) {
			return false;
		}
		
		return true;
	}
	
	private boolean hasSixDigits() {
		if (this.password.length() != 6) {
			return false;
		}
		
		return true;
	}
	
	private boolean hasIncreasingDigits() {
		boolean hasIncreasingDigits = true;
		
		for (int i = 0; i < this.password.length() - 1; i++) {
			int firstNumber = Integer.parseInt("" + this.password.charAt(i));
			int secondNumber = Integer.parseInt("" + this.password.charAt(i+1));
			
			if (firstNumber > secondNumber) {
				hasIncreasingDigits = false;
			}
		}
		
		return hasIncreasingDigits;
	}
	
	private boolean hasSameDigits() {
		for (int i = 0; i < this.password.length() - 1; i++) {
			int firstNumber = Integer.parseInt("" + this.password.charAt(i));
			int secondNumber = Integer.parseInt("" + this.password.charAt(i+1));
			
			if (firstNumber == secondNumber) {
				if (isLargerGroup(firstNumber)) {
					continue;
				}
				return true;
			}
			

		}
		
		return false;
	}
	
	private boolean isLargerGroup(int checkNumber) {
		int numberCounter = 0;
		for (int i = 0; i < this.password.length(); i++) {
			int number = Integer.parseInt("" + this.password.charAt(i));
			
			if (number == checkNumber) {
				numberCounter++;
			}
		}
		
		if (numberCounter > 2) {
			return true;
		}
		
		return false;
	}
	
}
