package year2019.day4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PasswordCheckerTest {

	@Test
	void test111122() {
		PasswordChecker passwordChecker = new PasswordChecker("111122");
		
		if (!passwordChecker.isValidPassword()) {
			fail("Password 111122 not valid");
		}
	}
	
	@Test
	void test112233() {
		PasswordChecker passwordChecker = new PasswordChecker("112233");
		
		if (!passwordChecker.isValidPassword()) {
			fail("Password 112233 not valid");
		}
	}
	
	@Test
	void test123444() {
		PasswordChecker passwordChecker = new PasswordChecker("123444");
		
		if (passwordChecker.isValidPassword()) {
			fail("Password 123444 should not be valid");
		}
	}

}
