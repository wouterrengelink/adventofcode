package year2019.day4;

import java.util.ArrayList;

public class Main {
	private static String input = "158126-624574";
			
	public static void main(String[] args) {
		System.out.println("Booting up");
		
		ArrayList<String> validPasswordList = new ArrayList<String>();
		
		for (int i = 158126; i <= 624575; i++) {
			PasswordChecker passwordChecker = new PasswordChecker("" + i);
			if (passwordChecker.isValidPassword()) {
				validPasswordList.add("" + i);
			}
		}
		
		System.out.println("Number of valid passwords " + validPasswordList.size());
		//It is a six-digit number.
		//The value is within the range given in your puzzle input.
		//Two adjacent digits are the same (like 22 in 122345).
		//Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
		System.out.println("Done");
	}

}
