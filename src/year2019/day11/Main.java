package year2019.day11;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.text.html.HTMLDocument.Iterator;

import general.InputReader;
import general.StringParser;

public class Main {
	
	private static String inputFile = "./src/year2019/day11/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Day 11 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		HashMap<Coordinate, Panel> shiphull = new HashMap<Coordinate, Panel>();
		
		Panel whitePanel = new Panel();
		whitePanel.setColor(Color.WHITE);
		
		shiphull.put(new Coordinate(0, 0), whitePanel);
		
		Robot robot = new Robot(program, shiphull);
		
		while(!robot.isHalted()) {
			robot.step();
		}
		
		int paintedPanels = 0;
		for (HashMap.Entry<Coordinate, Panel> entry : shiphull.entrySet()) {
			Panel panel = entry.getValue();
			
			if (panel.isPainted()) {
				paintedPanels++;
			}
		    //System.out.println(entry.getKey() + " = " + entry.getValue());
		}
		
		System.out.println(paintedPanels);
		
		ShiphullViewer shiphullViewer = new ShiphullViewer(shiphull);
		
		shiphullViewer.view();
		
		System.out.println("Done");
	}

}
