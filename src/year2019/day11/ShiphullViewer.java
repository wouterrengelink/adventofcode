package year2019.day11;

import java.awt.Canvas;
import java.awt.Graphics;
import java.util.HashMap;

import javax.swing.JFrame;

public class ShiphullViewer extends Canvas {
	private HashMap<Coordinate, Panel> shiphull;
	
	public ShiphullViewer(HashMap<Coordinate, Panel> shiphull) {
		this.shiphull = shiphull;
	}
	
	public void view() {
		JFrame frame = new JFrame("My Drawing");
        Canvas canvas = new ShiphullViewer(this.shiphull);
        canvas.setSize(1200, 500);
        canvas.setBackground(java.awt.Color.BLACK);
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
	}
	
	public void paint(Graphics g) {
		System.out.println(shiphull.size());
		for (HashMap.Entry<Coordinate, Panel> entry : shiphull.entrySet()) {
			Panel panel = entry.getValue();
			Coordinate coordinate = entry.getKey();
			
			System.out.println(coordinate.getX() + " " + coordinate.getY());
			if (panel.getColor() == Color.WHITE) {
				g.drawRect(coordinate.getX() * 20 + 100, coordinate.getY() * 20 + 300, 20, 20);
			}
			
		}
        
    }
	
}
