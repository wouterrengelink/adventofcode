package year2019.day11;

public enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN
}
