package year2019.day11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class Computer {
	private ArrayList<BigInteger> program;
	private ArrayList<BigInteger> initialProgram;
	private int instructionPointer;
	private ArrayList<BigInteger> inputs;
	private boolean isProgramHalted;
	private ParameterModeParser parameterModeParser;
	private BigInteger lastOutput;
	
	public Computer(ArrayList<BigInteger> program, int memorySize) {
		this.initialProgram = program;
		this.inputs = new ArrayList<BigInteger>();
		this.parameterModeParser = new ParameterModeParser();
		
		this.adjustMemory(memorySize);
		
		this.reset();
	}
	
	private void adjustMemory(int memorySize) {
		for (int i = this.initialProgram.size(); i < memorySize; i++) {
			this.initialProgram.add(new BigInteger("0"));
		}
	}
	
	public void reset() {
		this.isProgramHalted = false;
		this.program = new ArrayList<BigInteger>(this.initialProgram);
		this.inputs = new ArrayList<BigInteger>();
		this.instructionPointer = 0;
	}
	
	public void addInput(BigInteger value) {
		this.inputs.add(value);
	}
	
	public BigInteger run() {		
		while (!this.isProgramHalted) {
			BigInteger value = this.program.get(this.instructionPointer);
			
			Instruction instruction = InstructionParser.parse(value.intValue()); // <--- this could go wrong
			
			switch(instruction.getOpCode()) {
				case 1:
					this.add(instruction);
					this.instructionPointer += 4;
					break;
				case 2:
					this.multiply(instruction);
					this.instructionPointer += 4;
					break;
				case 3:
					this.requestInput(instruction);
					this.instructionPointer += 2;
					break;
				case 4:
					this.lastOutput = new BigInteger(this.showOutput(instruction));
					this.instructionPointer += 2;
					return this.lastOutput;
				case 5:
					if (!this.jumpIfTrue(instruction)) {
						this.instructionPointer += 3;
					}
					break;
				case 6:
					if (!this.jumpIfFalse(instruction)) {
						this.instructionPointer += 3;
					}
					break;
				case 7:
					this.lessThan(instruction);
					this.instructionPointer += 4;
					break;
				case 8:
					this.isEqual(instruction);
					this.instructionPointer += 4;
					break;
				case 9:
					this.adjustRelativeBase(instruction);
					this.instructionPointer += 2;
					break;
				default:
					this.isProgramHalted = true;
					this.instructionPointer += 1;
			}
		}
		
		return null;
	}
	
	public void adjustRelativeBase(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		
		BigInteger currentRelativeBase = this.parameterModeParser.getRelativeBase();
		
		BigInteger addedRelativeBase = currentRelativeBase.add(firstValue);
		
		this.parameterModeParser.setRelativeBase(addedRelativeBase);
	}
	
	public boolean isHalted() {
		return this.isProgramHalted;
	}
	
	public void setValues(int noun, int verb) {
		this.program.set(1, new BigInteger("" + noun));
		this.program.set(2, new BigInteger("" + verb));
	}
	
	private boolean jumpIfTrue(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		
		int compareValue = firstValue.compareTo(new BigInteger("0"));
		
		if (compareValue == 1) {
			this.instructionPointer = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition).intValue();
			
			return true;
		}
		
		return false;
	}
	
	private boolean jumpIfFalse(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		
		int compareValue = firstValue.compareTo(new BigInteger("0"));
		
		if (compareValue == 0) {
			this.instructionPointer = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition).intValue();
			
			return true;
		}
		
		return false;
	}
	
	private void lessThan(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		BigInteger resultPosition = this.program.get(this.instructionPointer + 3);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		BigInteger secondValue = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		int compareValue = firstValue.compareTo(secondValue);
		
		BigInteger finalPosition = this.parameterModeParser.getInputValue(this.program, instruction.getThirdParameterMode(), resultPosition);
		
		if (compareValue == -1) {
			this.program.set(finalPosition.intValue(), new BigInteger("1"));
		} else {
			this.program.set(finalPosition.intValue(), new BigInteger("0"));
		}
	}
	
	private void isEqual(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		BigInteger resultPosition = this.program.get(this.instructionPointer + 3);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		BigInteger secondValue = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		BigInteger finalPosition = this.parameterModeParser.getInputValue(this.program, instruction.getThirdParameterMode(), resultPosition);
		
		if (firstValue.equals(secondValue)) {
			this.program.set(finalPosition.intValue(), new BigInteger("1"));
		} else {
			this.program.set(finalPosition.intValue(), new BigInteger("0"));
		}
	}
	
	private void multiply(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		BigInteger resultPosition = this.program.get(this.instructionPointer + 3);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		BigInteger secondValue = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		BigInteger multipliedValue = firstValue.multiply(secondValue);
		
		BigInteger finalPosition = this.parameterModeParser.getInputValue(this.program, instruction.getThirdParameterMode(), resultPosition);
		
		this.program.set(finalPosition.intValue(), multipliedValue);
	}
	
	private void add(Instruction instruction) {
		BigInteger firstPosition = this.program.get(this.instructionPointer + 1);
		BigInteger secondPosition = this.program.get(this.instructionPointer + 2);
		BigInteger resultPosition = this.program.get(this.instructionPointer + 3);
		
		BigInteger firstValue = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		BigInteger secondValue = this.parameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		BigInteger addedValue = firstValue.add(secondValue);
		
		BigInteger finalPosition = this.parameterModeParser.getInputValue(this.program, instruction.getThirdParameterMode(), resultPosition);
		
		this.program.set(finalPosition.intValue(), addedValue);
	}
	
	private void requestInput(Instruction instruction) {
		BigInteger input = this.getInput();
		
		// this is the parameter for RELATIVE parameter mode
		BigInteger resultPosition = this.program.get(this.instructionPointer + 1);
		BigInteger finalPosition = this.parameterModeParser.getInputValue(this.program, instruction.getFirstParameterMode(), resultPosition);
		
		this.program.set(finalPosition.intValue(), input);
	}
	
	private BigInteger getInput() {
		BigInteger returnInput = this.inputs.get(0);
		
		this.inputs.remove(0);
		
		return returnInput;
	}
	
	private String showOutput(Instruction instruction) {
		BigInteger outputPosition = this.program.get(this.instructionPointer + 1);
		
		BigInteger value = this.parameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), outputPosition);
		
		System.out.println("Output: " + value);
		
		return "" + value;
	}
	
	public BigInteger getValueAtIndex(int index) {
		return this.program.get(index);
	}
	
	public BigInteger getValueAtZero() {
		return this.program.get(0);
	}
}
