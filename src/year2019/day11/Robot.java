package year2019.day11;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

public class Robot {
	private Computer computer;
	private Coordinate position;
	private HashMap<Coordinate, Panel> shiphull;
	private Direction direction;
	
	public Robot(ArrayList<BigInteger> program, HashMap<Coordinate, Panel> shiphull) {
		this.computer = new Computer(program, 2048);
		this.position = new Coordinate(0, 0);
		this.shiphull = shiphull;
		this.direction = Direction.UP;
	}
	
	public boolean isHalted() {
		return this.computer.isHalted();
	}
	
	public void step() {
		// read color of current panel
		Panel panel = this.shiphull.get(this.position);
		
		if (panel == null) {
			panel = this.createNewPanel();
		}

		this.computer.addInput(this.getInputFromPanel(panel));
		
		BigInteger colorAndCheck = computer.run();
		
		if (colorAndCheck == null) {
			this.shutdown();
			return;
		}
		
		int colorInteger = colorAndCheck.intValue();
		this.paintPanel(panel, colorInteger);
		
		int directionInteger = computer.run().intValue();
		this.moveRobot(directionInteger);
		
		this.setStep();
	}
	
	private void shutdown() {
		System.out.println("Robot finished painting");
	}
	
	private BigInteger getInputFromPanel(Panel panel) {
		BigInteger input = null;
		
		if (panel.getColor() == Color.BLACK) {
			input = new BigInteger("0");
		} else if (panel.getColor() == Color.WHITE) {
			input = new BigInteger("1");
		}
		
		return input;
	}
	
	private void paintPanel(Panel panel, int colorInteger) {
		if (colorInteger == 0) {
			if (panel.getColor() == Color.BLACK) {
				return;
			}
			panel.setColor(Color.BLACK);
		} else if (colorInteger == 1) {
			if (panel.getColor() == Color.WHITE) {
				return;
			}
			panel.setColor(Color.WHITE);
		} else {
			System.out.println("Unknown color integer from computer");
		}
	}
	
	private void moveRobot(int directionInteger) {
		if (directionInteger == 0) {
			this.moveLeft();
		} else if (directionInteger == 1) {
			this.moveRight();
		}
	}
	
	private void setStep() {
		int currentX = this.position.getX();
		int currentY = this.position.getY();
		
		switch (this.direction) {
			case DOWN:
				this.position.setY(currentY - 1);
				break;
			case LEFT:
				this.position.setX(currentX -1);
				break;
			case RIGHT:
				this.position.setX(currentX + 1);
				break;
			case UP:
				this.position.setY(currentY + 1);
				break;
			default:
				break;
		}
		
		System.out.println(this.position.toString());
	}
	
	private void moveLeft() {
		switch (this.direction) {
			case UP:
				this.direction = Direction.LEFT;
				break;
			case DOWN:
				this.direction = Direction.RIGHT;
				break;
			case LEFT:
				this.direction = Direction.DOWN;
				break;
			case RIGHT:
				this.direction = Direction.UP;
				break;
			default:
				break;
		}
	}
	
	private void moveRight() {
		switch (this.direction) {
			case UP:
				this.direction = Direction.RIGHT;
				break;
			case DOWN:
				this.direction = Direction.LEFT;
				break;
			case LEFT:
				this.direction = Direction.UP;
				break;
			case RIGHT:
				this.direction = Direction.DOWN;
				break;
			default:
				break;
		}
	}
	
	private Panel createNewPanel() {
		Panel panel = new Panel();
		
		this.shiphull.put(new Coordinate(this.position.getX(), this.position.getY()), panel);
		
		return panel;
	}

}
