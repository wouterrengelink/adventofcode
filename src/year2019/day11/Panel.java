package year2019.day11;

public class Panel {
	private Color color;
	private boolean isPainted;
	
	public Panel() {
		this.color = Color.BLACK;
		this.isPainted = false;
	}
	
	public void setColor(Color color) {
		this.color = color;
		this.isPainted = true;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public boolean isPainted() {
		return this.isPainted;
	}
	
}
