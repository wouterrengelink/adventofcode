package year2019.day11;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
