package year2019.day3.take1;

import java.util.ArrayList;

public class MinimalDistanceFinder {
	
	public static int findMinimalDistance(ArrayList<Intersection> intersectionList) {
		int minimumDistance = 9999999;
		
		for (Intersection intersection : intersectionList) {
			int distance = ManhattanDistanceCalculator.calculateManhattanDistance(intersection.getCoordinate());
		
			if (distance < minimumDistance) {
				minimumDistance = distance;
			}
		}
		
		return minimumDistance;
	}
}
