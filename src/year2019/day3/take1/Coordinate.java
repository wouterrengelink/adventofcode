package year2019.day3.take1;

public class Coordinate {
	private int x;
	private int y;
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public String toString() {
		return x + "-" + y;
	}
	
	public boolean isEqual(Coordinate coordinate) {
		if (coordinate.x == x && coordinate.y == y) {
			return true;
		}
		return false;
	}
}
