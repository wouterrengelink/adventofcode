package year2019.day3.take1;

public class ManhattanDistanceCalculator {

	public static int calculateManhattanDistance(Coordinate coordinate) {		
		int manhattanX = Math.abs(coordinate.getX());
		int manhattanY = Math.abs(coordinate.getY());
		
		return manhattanX + manhattanY;
	}
	
}
