package year2019.day3.take1;

public enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN
}
