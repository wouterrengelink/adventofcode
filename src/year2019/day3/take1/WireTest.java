package year2019.day3.take1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class WireTest {

	@Test
	void test159() {
		Wire wireA = new Wire(1);
		
		wireA.addPathStep(new Vector("R75"));
		wireA.addPathStep(new Vector("D30"));
		wireA.addPathStep(new Vector("R83"));
		wireA.addPathStep(new Vector("U83"));
		wireA.addPathStep(new Vector("L12"));
		wireA.addPathStep(new Vector("D49"));
		wireA.addPathStep(new Vector("R71"));
		wireA.addPathStep(new Vector("U7"));
		wireA.addPathStep(new Vector("L72"));
		
		Wire wireB = new Wire(2);
		wireB.addPathStep(new Vector("U62"));
		wireB.addPathStep(new Vector("R66"));
		wireB.addPathStep(new Vector("U55"));
		wireB.addPathStep(new Vector("R34"));
		wireB.addPathStep(new Vector("D71"));
		wireB.addPathStep(new Vector("R55"));
		wireB.addPathStep(new Vector("D58"));
		wireB.addPathStep(new Vector("R83"));
		
		IntersectionRunner intersectionRunner = new IntersectionRunner(wireA, wireB);
		
		ArrayList<Intersection> intersectionList = intersectionRunner.getIntersections();
		
		int minimalSteps = MinimalStepFinder.find(intersectionList);

		if (minimalSteps != 610) {
			fail("Steps 610 test failed");
		}
		
		if (MinimalDistanceFinder.findMinimalDistance(intersectionList) != 159) {
			fail("Minimal distance 159 failed");
		}
		
	}
	
	@Test
	void test135() {
		Wire wireA = new Wire(3);
		
		wireA.addPathStep(new Vector("R98"));
		wireA.addPathStep(new Vector("U47"));
		wireA.addPathStep(new Vector("R26"));
		wireA.addPathStep(new Vector("D63"));
		wireA.addPathStep(new Vector("R33"));
		wireA.addPathStep(new Vector("U87"));
		wireA.addPathStep(new Vector("L62"));
		wireA.addPathStep(new Vector("D20"));
		wireA.addPathStep(new Vector("R33"));
		wireA.addPathStep(new Vector("U53"));
		wireA.addPathStep(new Vector("R51"));
		
		Wire wireB = new Wire(4);
		wireB.addPathStep(new Vector("U98"));
		wireB.addPathStep(new Vector("R91"));
		wireB.addPathStep(new Vector("D20"));
		wireB.addPathStep(new Vector("R16"));
		wireB.addPathStep(new Vector("D67"));
		wireB.addPathStep(new Vector("R40"));
		wireB.addPathStep(new Vector("U7"));
		wireB.addPathStep(new Vector("R15"));
		wireB.addPathStep(new Vector("U6"));
		wireB.addPathStep(new Vector("R7"));
		
		IntersectionRunner intersectionRunner = new IntersectionRunner(wireA, wireB);
		
		ArrayList<Intersection> intersectionList = intersectionRunner.getIntersections();
		
		int minimalSteps = MinimalStepFinder.find(intersectionList);

		if (minimalSteps != 410) {
			fail("Steps 410 test failed");
		}
		
		if (MinimalDistanceFinder.findMinimalDistance(intersectionList) != 135) {
			fail("Minimal distance 135 failed");
		}
		
	}

}
