package year2019.day3.take1;

import java.util.ArrayList;

public class MinimalStepFinder {
	
	public static int find(ArrayList<Intersection> intersections) {
		int minimalSteps = 99999999;
		
		for (Intersection intersection : intersections) {
			int totalSteps = intersection.getAddedSteps();

			if (totalSteps < minimalSteps) {
				minimalSteps = totalSteps;
			}
		}
		
		return minimalSteps;
	}
}
