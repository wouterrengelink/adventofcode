package year2019.day3.take1;

import java.util.ArrayList;

public class WireRunner {
	private Wire wire;
	private int x;
	private int y;
	private int wirenumber;
	private ArrayList<Coordinate> wireCoordinates;
	
	public WireRunner( Wire wire) {
		this.wire = wire;
		this.wirenumber = wire.getWireNumber();
		this.wireCoordinates = new ArrayList<Coordinate>();;
		this.x = 0;
		this.y = 0;
	}
	
	public void runWire() {		
		for (Vector vector : this.wire.getPathList()) {
			this.move(vector.getDirection(), vector.getValue());
		}
	}
	
	private void move(Direction direction, int steps) {
		for (int i = steps; i > 0; i--) {
			
			switch(direction) {
				case LEFT:
					x--;
					break;
				case RIGHT:
					x++;
					break;
				case DOWN:
					y++;
					break;
				case UP:
					y--;
					break;
			}
		
			Coordinate newCoordinate = new Coordinate(x, y);
			boolean coordinteExists = false;
			for (Coordinate coordinate : this.wireCoordinates) {
				if (newCoordinate.isEqual(coordinate)) {
					coordinteExists = true;
				}
			}
			
			if (!coordinteExists) {
				this.wireCoordinates.add(newCoordinate);
			}
		}
	}
	
	public ArrayList<Coordinate> getCoordinates() {
		return this.wireCoordinates;
	}
}
