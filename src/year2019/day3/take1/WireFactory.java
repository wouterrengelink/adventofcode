package year2019.day3.take1;

import java.util.ArrayList;

public class WireFactory {
	private static int wireserialnumber = 1;
	
	public static Wire constructWire(ArrayList<String> wireList) {
		Wire wire = new Wire(wireserialnumber++);
		
		for (String vectorString : wireList) {
			wire.addPathStep(new Vector(vectorString));
		}
		
		return wire;
	}
}
