package year2019.day3.take1;

import java.util.ArrayList;

public class IntersectionRunner {
	private Wire wireA;
	private Wire wireB;
	
	public IntersectionRunner(Wire wireA, Wire wireB) {
		this.wireA = wireA;
		this.wireB = wireB;
	}
	
	public ArrayList<Intersection> getIntersections() {
		WireRunner wireARunner = new WireRunner(wireA);
		WireRunner wireBRunner = new WireRunner(wireB);
		
		wireARunner.runWire();
		wireBRunner.runWire();
		
		ArrayList<Coordinate> ranWireACoordinates = wireARunner.getCoordinates();
		ArrayList<Coordinate> ranWireBCoordinates = wireBRunner.getCoordinates();
		
		IntersectionFinder intersectionFinder = new IntersectionFinder(ranWireACoordinates, ranWireBCoordinates);
		
		return intersectionFinder.findIntersections();
	}
}
