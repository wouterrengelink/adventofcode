package year2019.day3.take1;

import java.util.ArrayList;

public class IntersectionFinder {
	private ArrayList<Coordinate> wireACoordinates;
	private ArrayList<Coordinate> wireBCoordinates;
	
	public IntersectionFinder(ArrayList<Coordinate> wireACoordinates, ArrayList<Coordinate> wireBCoordinates) {
		this.wireACoordinates = wireACoordinates;
		this.wireBCoordinates = wireBCoordinates;
	}
	
	public ArrayList<Intersection> findIntersections() {
		ArrayList<Intersection> intersectionCoordinates = new ArrayList<Intersection>();
		for (int i = 0; i < wireACoordinates.size(); i++) {
			for (int j = 0; j < wireBCoordinates.size(); j++) {
				if (wireACoordinates.get(i).isEqual(wireBCoordinates.get(j))) {
					// find lowest i for coordinate
					// find lowest j for coordinate
					int stepA = this.findSmallestStepCount(wireACoordinates, wireACoordinates.get(i)) + 1;
					int stepB = this.findSmallestStepCount(wireBCoordinates, wireBCoordinates.get(j)) + 1;
					
					Intersection intersection = new Intersection(wireACoordinates.get(i), stepA, stepB);
					
					intersectionCoordinates.add(intersection);
				}
			}
		}
		return intersectionCoordinates;

	}
	
	private int findSmallestStepCount(ArrayList<Coordinate> wireCoordinates, Coordinate checkCoordinate) {
		for (int i = 0; i < wireCoordinates.size(); i++) {
			if (wireCoordinates.get(i).isEqual(checkCoordinate)) {
				return i;
			}
		}
		
		return -1;
	}
	
}
