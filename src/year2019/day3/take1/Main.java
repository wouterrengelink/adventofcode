package year2019.day3.take1;

import java.util.ArrayList;
 
import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day3/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Booting up intersection finder");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> directions = inputReader.getInputFromSeparator("\n");
		
		ArrayList<String> wireAContent = StringParser.splitOnSeparator(directions.get(0), ",");
		ArrayList<String> wireBContent = StringParser.splitOnSeparator(directions.get(1), ",");
		
		Wire wireA = WireFactory.constructWire(wireAContent);
		Wire wireB = WireFactory.constructWire(wireBContent);

		IntersectionRunner intersectionRunner = new IntersectionRunner(wireA, wireB);

		ArrayList<Intersection> intersections = intersectionRunner.getIntersections();
		int smallestSteps = MinimalStepFinder.find(intersections); 
		System.out.println("Smallest steps: " + smallestSteps);

		System.out.println("Program done");
	}

}
