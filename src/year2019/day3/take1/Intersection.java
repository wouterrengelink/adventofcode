package year2019.day3.take1;

public class Intersection {
	private Coordinate coordinate;

	private int stepA;
	private int stepB;
	
	public Intersection(Coordinate coordinate, int stepA, int stepB) {
		this.coordinate = coordinate;
		this.stepA = stepA;
		this.stepB = stepB;
	}
	
	public int getAddedSteps() {
		return this.stepA + this.stepB;
	}
	
	public Coordinate getCoordinate() {
		return this.coordinate;
	}
	
}
