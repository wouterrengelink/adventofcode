package year2019.day3.take1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Vector {
	private int value;
	private Direction direction;
	
	public Vector(String coordinate) {
		this.parseCoordinate(coordinate);
	}
	
	private void parseCoordinate(String coordinate) {
		String pattern = "([A-Z])(\\d+)";
		
		Pattern regex = Pattern.compile(pattern);

	    // Now create matcher object.
	    Matcher matcher = regex.matcher(coordinate);
	      
	    if (matcher.find( )) {
	    	this.setDirection(matcher.group(1));
	    	this.setValue(matcher.group(2));
	    } else {
	       System.out.println("NO MATCH");
	    }
	}
	
	private void setDirection(String direction) {
		switch (direction) {
			case "U":
				this.direction = Direction.UP;
				break;
			case "D":
				this.direction = Direction.DOWN;
				break;
			case "L":
				this.direction = Direction.LEFT;
				break;
			case "R":
				this.direction = Direction.RIGHT;
		}
	}
	
	private void setValue(String value) {
		this.value = Integer.parseInt(value);
	}
	
	public Direction getDirection() {
		return this.direction;
	}
	
	public int getValue() {
		return this.value;
	}
}
