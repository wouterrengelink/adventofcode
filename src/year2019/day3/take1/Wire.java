package year2019.day3.take1;

import java.util.ArrayList;

public class Wire {
	private ArrayList<Vector> pathList;
	private int wireNumber;
	
	public Wire(int wireNumber) {
		this.pathList = new ArrayList<Vector>();
		this.wireNumber = wireNumber;
	}
	
	public void addPathStep(Vector vector) {
		this.pathList.add(vector);
	}
	
	public ArrayList<Vector> getPathList() {
		return this.pathList;
	}
	
	public int getWireNumber() {
		return this.wireNumber;
	}

}
