package year2019.day3.take2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import general.InputReader;
import general.StringParser;

public class Main {
	
	private static String inputFile = "./src/year2019/day3/input.txt";

	public static void main(String[] args) {
		System.out.println("Take two using Set");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> directions = inputReader.getInputFromSeparator("\n");
		
		ArrayList<String> wireAContent = StringParser.splitOnSeparator(directions.get(0), ",");
		ArrayList<String> wireBContent = StringParser.splitOnSeparator(directions.get(1), ",");
		
		Set<Coordinate> wireASet = new HashSet<Coordinate>();
		Set<Coordinate> wireBSet = new HashSet<Coordinate>();
		
		Walker aWalker = new Walker(wireASet);
		aWalker.walk(wireAContent);
		
		Walker bWalker = new Walker(wireBSet);
		bWalker.walk(wireBContent);
		
		Set<Coordinate> aSet = aWalker.getSet();
		Set<Coordinate> bSet = bWalker.getSet();
		
		HashSet<Coordinate> interSectionSet = new HashSet<Coordinate>(aSet);
		
		interSectionSet.retainAll(bSet);
		
		int minimalSteps = 99999999;
		for (Coordinate intersection : interSectionSet) {
			int aSteps = -1;
			int bSteps = -1;
			
			for (Coordinate aCoordinate : aSet) {
				if (aCoordinate.equals(intersection)) {
					aSteps = aCoordinate.getSteps();
				}
			}
			
			for (Coordinate bCoordinate : bSet) {
				if (bCoordinate.equals(intersection)) {
					bSteps = bCoordinate.getSteps();
				}
			}
			
			int totalSteps = aSteps + bSteps;
			
			if (totalSteps < minimalSteps) {
				minimalSteps = totalSteps;
			}
		
		}
		
		System.out.println("steps for intersection " + minimalSteps);
		
		int minimalDistance = 99999999;
		for (Coordinate coordinate : aSet) {
			int manhattanDistance = ManhattanDistanceCalculator.calculateManhattanDistance(coordinate);
			
			if (manhattanDistance < minimalDistance) {
				minimalDistance = manhattanDistance;
			}
		}
		
		System.out.println("Minimal manhattan distance: " + minimalDistance);
		System.out.println("Program finished");
	}

}
