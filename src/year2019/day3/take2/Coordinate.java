package year2019.day3.take2;

public class Coordinate {
	private int x;
	private int y;
	private int steps;
	
	public Coordinate(int x, int y, int steps) {
		this.x = x;
		this.y = y;
		this.steps = steps;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getSteps() {
		return this.steps;
	}
	
	@Override
    public boolean equals(Object object) {
       if (!(object instanceof Coordinate))
            return false;
        if (object == this)
            return true;
        
        Coordinate coordinate = (Coordinate) object;
        
        if (this.x == coordinate.getX() && this.y == coordinate.getY()) {
        	return true;
        }
        
        return false;
    }
	
	@Override
    public int hashCode() {
		String coordinateString = this.x + "_" + this.y;
		
		return coordinateString.hashCode();
    }
	
}
