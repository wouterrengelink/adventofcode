package year2019.day3.take2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

class WalkerTest {

	@Test
	void steps610test() {
		// THIS TEST WAS ABANDONED BECAUSE I PASSED THE CHALLENGE
		
		ArrayList<String> pathA = new ArrayList<String>();
		ArrayList<String> pathB = new ArrayList<String>();
		
		pathA.add("R75");
		pathA.add("D30");
		pathA.add("R83");
		pathA.add("U83");
		pathA.add("L12");
		pathA.add("D49");
		pathA.add("R71");
		pathA.add("U7");
		pathA.add("L72");
		
		pathB.add("U62");
		pathB.add("R66");
		pathB.add("U55");
		pathB.add("R34");
		pathB.add("D71");
		pathB.add("R55");
		pathB.add("D58");
		pathB.add("R83");

		HashSet<Coordinate> setA = new HashSet<Coordinate>();
		HashSet<Coordinate> setB = new HashSet<Coordinate>();
		
		Walker walkerA = new Walker(setA);
		Walker walkerB = new Walker(setB);
		
		walkerA.walk(pathA);
		walkerB.walk(pathB);
		
		HashSet<Coordinate> interSectionSet = new HashSet<Coordinate>(setA);
		
		interSectionSet.retainAll(setB);
		
//		for (Coordinate intersection)
		
		//setA.retainAll(setB);
		
		for (Coordinate intersection : setA) {
			System.out.println("" + intersection.getSteps());
		}
	}

}
