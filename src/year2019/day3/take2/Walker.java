package year2019.day3.take2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Walker {
	private Set<Coordinate> set;
	private int x;
	private int y;
	private int totalSteps;
	
	public Walker(Set<Coordinate> set) {
		this.set = set;
		this.x = 0;
		this.y = 0;
		this.totalSteps = 0;
	}
	
	public void walk(ArrayList<String> path) {
		for (String instruction : path) {
			// regular expression
			String regex = "([A-Z])(\\d+)";

			Pattern pattern = Pattern.compile(regex);
		    Matcher matcher = pattern.matcher(instruction);
		    
		    if (matcher.find()) {
			    String direction = matcher.group(1);
			    int steps = Integer.parseInt(matcher.group(2));
			    
			    this.parseInstruction(direction, steps);
		    } else {
		       System.out.println("NO MATCH");
		    }
		}
	}
	
	private void parseInstruction(String direction, int steps) {
		for (int i = 0; i < steps; i++) {
			this.totalSteps++;
			
			switch(direction) {
				case "U":
					this.y++;
					break;
				case "D":
					this.y--;
					break;
				case "L":
					this.x--;
					break;
				case "R":
					this.x++;
					break;
			}
			
			Coordinate coordinate = new Coordinate(this.x, this.y, this.totalSteps);
			
			if (!this.set.contains(coordinate)) {
				this.set.add(coordinate);
			}
		}
	}
	
	public Set<Coordinate> getSet() {
		return this.set;
	}
	
}
