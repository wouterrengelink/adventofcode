package year2019.day24;

import java.util.ArrayList;

import general.InputReader;

public class Main {
	private static String inputFile = "./src/year2019/day24/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Day 24 2019");

		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> linesList = inputReader.getInputFromList();
		
		World world = new World(5, 5, 250, null);
		
		world.fillWorld(linesList);
		
		//world.showWorld();
		for (int i = 0; i < 200; i++) {
			world.processMinute();
			world.updateWorld();
			System.out.println("Number of bugs " + world.totalNumberOfBugs());
		}
		
		System.out.println("Number of bugs " + world.totalNumberOfBugs());
		
		System.out.println("Done");
	}

}
