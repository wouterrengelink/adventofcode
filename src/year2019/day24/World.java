package year2019.day24;

import java.util.ArrayList;

public class World {
	private int width;
	private int height;
	private Map map;
	private ArrayList<Coordinate> killList;
	private ArrayList<Coordinate> growList;
	private boolean previousStateFound;
	private ArrayList<Map> previousMaps;
	private ArrayList<Integer> valueList;
	private int depth;
	private boolean isChildWorld;
	
	private World parentWorld;
	private World childWorld;
	
	public World(int width, int height, int depth, World childWorld) {
		this.width = width;
		this.height = height;
		this.map = new Map(width, height);
		this.killList = new ArrayList<Coordinate>();
		this.growList = new ArrayList<Coordinate>();
		this.depth = depth;
		this.previousMaps = new ArrayList<Map>();
		this.previousStateFound = false;
		this.isChildWorld = false;
		this.valueList = new ArrayList<Integer>();
		
		if (childWorld != null) {
			this.childWorld = childWorld;
		}
		
		depth--;
		if (depth > 0) {
			if (this.childWorld == null) {
				this.childWorld = new World(width, height, depth, null);
				this.childWorld.setWorldToEmpty();
				this.childWorld.setChildWorld(true);
				this.childWorld.setParentWorld(this);
				this.childWorld.setDepth(depth);
				//this.childWorld.showWorld();
			}
			
			if (this.parentWorld == null) {
				this.parentWorld = new World(this.width, this.height, depth, this);
				this.parentWorld.setWorldToEmpty();
				this.parentWorld.setDepth(-1 * depth);
				//this.parentWorld.showWorld();
			}
		}
	}
	
	public void setChildWorld(boolean isChildWorld) {
		this.isChildWorld = isChildWorld;
	}
	
	public void setParentWorld(World world) {
		this.parentWorld = world;
	}
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	public void setWorldToEmpty() {
		for (int x = 0; x < this.width; x++)
		for (int y = 0; y < this.height; y++) {
			if (x == 2 && y == 2) {
				this.map.setTile(new Tile(TileType.RECURSIVE_WORLD), x, y);
			} else {
				this.map.setTile(new Tile(TileType.EMPTY), x, y);
			}
		}
	}

	public void fillWorld(ArrayList<String> linesList) {
		int yIterator = 0;
		
		for (String line : linesList) {
			this.parseLine(line, yIterator);
			yIterator++;
		}
	}
	
	private void parseLine(String line, int yIterator) {
		int xIterator = 0;
		for (char character : line.toCharArray()) {
			
			if (yIterator == 2 && xIterator == 2) {
				this.map.setTile(new Tile(TileType.RECURSIVE_WORLD), xIterator, yIterator);
			} else if (character == '#') {
				this.map.setTile(new Tile(TileType.BUG), xIterator, yIterator);
			} else {
				this.map.setTile(new Tile(TileType.EMPTY), xIterator, yIterator);
			}
			xIterator++;
		}
	}
	
	public boolean previousStateFound() {
		return this.previousStateFound;
	}
	
	public void processMinute() {		
		if (!this.isChildWorld && this.parentWorld != null) {
			this.parentWorld.processMinute();
		}
		
		for (int x = 0; x < this.width; x++)
		for (int y = 0; y < this.height; y++) {
			
			if (this.childWorld != null) {
				if (x == 1 && y == 2) {
					this.handleLeftFromGrid(x, y);
					continue;
				} else if (x == 2 && y == 1) { 
					this.handleTopFromGrid(x, y);
					continue;
				} else if (x == 2 && y == 3) { 
					this.handleBottomFromGrid(x, y);
					continue;
				} else if (x == 3 && y == 2) { 
					this.handleRightFromGrid(x, y);
					continue;
				}
			}
			
			Tile tile = this.map.getTile(x, y);
		
			switch (tile.getType()) {
				case BUG:
					this.handleBug(x, y);
					break;
				case EMPTY:
					this.handleEmpty(x, y);
					break;
				case RECURSIVE_WORLD:
					break;
			}
		}
		
		if (this.childWorld != null && this.childWorld.isChildWorld) {
			this.childWorld.processMinute();
		}
	}
	
	public void updateWorld() {
		this.handleKillList();
		this.handleGrowList();
		
		if (!this.isChildWorld && this.parentWorld != null) {
			this.parentWorld.updateWorld();
		}
		
		if (this.childWorld != null && this.childWorld.isChildWorld) {
			this.childWorld.updateWorld();
		}
	}
	
	private void handleLeftFromGrid(int x, int y) {
		ArrayList<Tile> tilesList = this.childWorld.getLeftTiles();
		
		int numberOfBugs = World.countBugs(tilesList);
		
		Tile currentTile = this.getTileOnPosition(x, y);
		
		Tile leftTile = this.getTileOnPosition(x - 1, y);
		Tile topTile = this.getTileOnPosition(x, y - 1);
		Tile bottomTile = this.getTileOnPosition(x, y + 1);
		
		if (leftTile != null && leftTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (topTile != null && topTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (bottomTile != null && bottomTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		currentTile.setAdjacentBugs(numberOfBugs);
		
		if (currentTile.getType() == TileType.BUG) {
			if (numberOfBugs != 1) {
				this.killList.add(new Coordinate(x, y));
			}
		} else {
			if (numberOfBugs == 1 || numberOfBugs == 2) {
				this.growList.add(new Coordinate(x, y));
			}
		}
	}
	
	private void handleTopFromGrid(int x, int y) {
		ArrayList<Tile> tilesList = this.childWorld.getTopTiles();
		
		int numberOfBugs = World.countBugs(tilesList);
		
		Tile currentTile = this.getTileOnPosition(x, y);
		
		Tile leftTile = this.getTileOnPosition(x - 1, y);
		Tile topTile = this.getTileOnPosition(x, y - 1);
		Tile rightTile = this.getTileOnPosition(x + 1, y);
		
		if (leftTile != null && leftTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (topTile != null && topTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (rightTile != null && rightTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		currentTile.setAdjacentBugs(numberOfBugs);
		
		if (currentTile.getType() == TileType.BUG) {
			if (numberOfBugs != 1) {
				this.killList.add(new Coordinate(x, y));
			}
		} else {
			if (numberOfBugs == 1 || numberOfBugs == 2) {
				this.growList.add(new Coordinate(x, y));
			}
		}
	}
	
	private void handleRightFromGrid(int x, int y) {
		ArrayList<Tile> tilesList = this.childWorld.getRightTiles();
		
		int numberOfBugs = World.countBugs(tilesList);
		
		Tile currentTile = this.getTileOnPosition(x, y);
		
		Tile rightTile = this.getTileOnPosition(x + 1, y);
		Tile topTile = this.getTileOnPosition(x, y - 1);
		Tile bottomTile = this.getTileOnPosition(x, y + 1);
		
		if (rightTile != null && rightTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (topTile != null && topTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (bottomTile != null && bottomTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		currentTile.setAdjacentBugs(numberOfBugs);
		
		if (currentTile.getType() == TileType.BUG) {
			if (numberOfBugs != 1) {
				this.killList.add(new Coordinate(x, y));
			}
		} else {
			if (numberOfBugs == 1 || numberOfBugs == 2) {
				this.growList.add(new Coordinate(x, y));
			}
		}
	}
	
	private void handleBottomFromGrid(int x, int y) {
		ArrayList<Tile> tilesList = this.childWorld.getBottomTiles();
		
		int numberOfBugs = World.countBugs(tilesList);
		
		Tile currentTile = this.getTileOnPosition(x, y);
		
		Tile leftTile = this.getTileOnPosition(x - 1, y);
		Tile rightTile = this.getTileOnPosition(x + 1, y);
		Tile bottomTile = this.getTileOnPosition(x, y + 1);
		
		if (leftTile != null && leftTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (rightTile != null && rightTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		if (bottomTile != null && bottomTile.getType() == TileType.BUG) {
			numberOfBugs++;
		}
		
		currentTile.setAdjacentBugs(numberOfBugs);
		
		if (currentTile.getType() == TileType.BUG) {
			if (numberOfBugs != 1) {
				this.killList.add(new Coordinate(x, y));
			}
		} else {
			if (numberOfBugs == 1 || numberOfBugs == 2) {
				this.growList.add(new Coordinate(x, y));
			}
		}
	}
	
	private void handleKillList() {
		for (Coordinate coordinate : this.killList) {
			this.map.setTile(new Tile(TileType.EMPTY), coordinate.getX(), coordinate.getY());
		}
		
		this.killList = new ArrayList<Coordinate>();
	}
	
	private static int countBugs(ArrayList<Tile> tilesList) {
		int numberOfBugs = 0;
		
		for (Tile tile : tilesList) {
			if (tile.getType() == TileType.BUG) {
				numberOfBugs++;
			}
		}
		
		return numberOfBugs;
	}
	
	public Tile getTopFromGrid() {
		return this.getTileOnPosition(2, 1);
	}
	
	public Tile getLeftFromGrid() {
		return this.getTileOnPosition(1, 2);
	}
	
	public Tile getRightFromGrid() {
		return this.getTileOnPosition(3, 2);
	}
	
	public Tile getBottomFromGrid() {
		return this.getTileOnPosition(2, 3);
	}
	
	private void handleGrowList() {
		for (Coordinate coordinate : this.growList) {
			this.map.setTile(new Tile(TileType.BUG), coordinate.getX(), coordinate.getY());
		}
		
		this.growList = new ArrayList<Coordinate>();
	}
	
	private void handleEmpty(int x, int y) {
		int numberOfBugsAdjencant = 0;
		
		Tile leftTile = this.getTileOnPosition(x - 1, y);
		Tile topTile = this.getTileOnPosition(x, y - 1);
		Tile rightTile = this.getTileOnPosition(x + 1, y);
		Tile bottomTile = this.getTileOnPosition(x, y + 1);
		
		if (this.parentWorld != null) {
			if (leftTile == null) {
				leftTile = this.parentWorld.getLeftFromGrid();
			}
			if (topTile == null) {
				topTile = this.parentWorld.getTopFromGrid();
			}
			if (rightTile == null) {
				rightTile = this.parentWorld.getRightFromGrid();
			} 
			if (bottomTile == null) {
				bottomTile = this.parentWorld.getBottomFromGrid();
			}
		}
		
		if (leftTile != null && leftTile.getType() == TileType.BUG) {
			numberOfBugsAdjencant++;
		}
		if (topTile != null && topTile.getType() == TileType.BUG) {
			numberOfBugsAdjencant++;
		}
		if (rightTile != null && rightTile.getType() == TileType.BUG) {
			numberOfBugsAdjencant++;
		}
		if (bottomTile != null && bottomTile.getType() == TileType.BUG) {
			numberOfBugsAdjencant++;
		}
		
		Tile tile = this.getTileOnPosition(x, y);
		tile.setAdjacentBugs(numberOfBugsAdjencant);

		if (numberOfBugsAdjencant == 1 || numberOfBugsAdjencant == 2) {
			this.growList.add(new Coordinate(x, y));
		}
	}
	
	private void handleBug(int x, int y) {
		int numberOfBugsAsNeighbour = 0;
		
		Tile leftTile = this.getTileOnPosition(x - 1, y);
		Tile topTile = this.getTileOnPosition(x, y - 1);
		Tile rightTile = this.getTileOnPosition(x + 1, y);
		Tile bottomTile = this.getTileOnPosition(x, y + 1);
		
		if (this.parentWorld != null) {
			if (leftTile == null) {
				leftTile = this.parentWorld.getLeftFromGrid();
			}
			if (topTile == null) {
				topTile = this.parentWorld.getTopFromGrid();
			}
			if (rightTile == null) {
				rightTile = this.parentWorld.getRightFromGrid();
			} 
			if (bottomTile == null) {
				bottomTile = this.parentWorld.getBottomFromGrid();
			}
		}
		
		if (leftTile != null && leftTile.getType() == TileType.BUG) {
			numberOfBugsAsNeighbour++;
		}
		if (topTile != null && topTile.getType() == TileType.BUG) {
			numberOfBugsAsNeighbour++;
		}
		if (rightTile != null && rightTile.getType() == TileType.BUG) {
			numberOfBugsAsNeighbour++;
		}
		if (bottomTile != null && bottomTile.getType() == TileType.BUG) {
			numberOfBugsAsNeighbour++;
		}
		
		Tile tile = this.getTileOnPosition(x, y);
		tile.setAdjacentBugs(numberOfBugsAsNeighbour);
		
		if (numberOfBugsAsNeighbour != 1) {
			this.killList.add(new Coordinate(x, y));
		}
	}
	
	public void showWorld() {
		MapViewer mapViewer = new MapViewer(this);
	}
	
	public int getDepth() {
		return this.depth;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public Tile getTileOnPosition(int x, int y) {
		return this.map.getTile(x, y);
	}
	
	public ArrayList<Tile> getTopTiles() {
		ArrayList<Tile> tilesList = new ArrayList<Tile>();
		
		tilesList.add(this.map.getTiles()[0][0]);
		tilesList.add(this.map.getTiles()[1][0]);
		tilesList.add(this.map.getTiles()[2][0]);
		tilesList.add(this.map.getTiles()[3][0]);
		tilesList.add(this.map.getTiles()[4][0]);
		
		return tilesList;
	}
	
	public ArrayList<Tile> getBottomTiles() {
		ArrayList<Tile> tilesList = new ArrayList<Tile>();
		
		tilesList.add(this.map.getTiles()[0][4]);
		tilesList.add(this.map.getTiles()[1][4]);
		tilesList.add(this.map.getTiles()[2][4]);
		tilesList.add(this.map.getTiles()[3][4]);
		tilesList.add(this.map.getTiles()[4][4]);
		
		return tilesList;
	}
	
	public ArrayList<Tile> getLeftTiles() {
		ArrayList<Tile> tilesList = new ArrayList<Tile>();
		
		tilesList.add(this.map.getTiles()[0][0]);
		tilesList.add(this.map.getTiles()[0][1]);
		tilesList.add(this.map.getTiles()[0][2]);
		tilesList.add(this.map.getTiles()[0][3]);
		tilesList.add(this.map.getTiles()[0][4]);
		
		return tilesList;
	}
	
	public ArrayList<Tile> getRightTiles() {
		ArrayList<Tile> tilesList = new ArrayList<Tile>();
		
		tilesList.add(this.map.getTiles()[4][0]);
		tilesList.add(this.map.getTiles()[4][1]);
		tilesList.add(this.map.getTiles()[4][2]);
		tilesList.add(this.map.getTiles()[4][3]);
		tilesList.add(this.map.getTiles()[4][4]);
		
		return tilesList;
	}
	
	public int totalNumberOfBugs() {
		int numberOfBugs = this.numberOfBugsInThisWorld();
		
		// get bugs of parents
		World parentWorld = this.parentWorld;
		
		while (parentWorld != null) {
			numberOfBugs += parentWorld.numberOfBugsInThisWorld();
			parentWorld = parentWorld.parentWorld;
		}
		
		// get bugs of childs
		World childWorld = this.childWorld;
		
		while (childWorld != null) {
			numberOfBugs += childWorld.numberOfBugsInThisWorld();
			childWorld = childWorld.childWorld;
		}
		
		
		return numberOfBugs;
	}
	
	public int numberOfBugsInThisWorld() {
		int numberOfBugs = 0;
		
		for (int x = 0; x < this.getWidth(); x++)
		for (int y = 0; y < this.getHeight(); y++) {
			Tile tile = this.getTileOnPosition(x, y);
			
			if (tile.getType() == TileType.BUG) {
				numberOfBugs++;
			}
		}
		
		return numberOfBugs;
	}
}
