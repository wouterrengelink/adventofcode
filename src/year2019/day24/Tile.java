package year2019.day24;

public class Tile {
	private TileType tileType;
	private int adjacentBugs;
	
	public Tile(Tile tile) {
		this.tileType = tile.getType();
		this.adjacentBugs = 0;
	}
	
	public Tile(TileType tileType) {
		this.tileType = tileType;
	}
	
	public TileType getType() {
		return this.tileType;
	}
	
	public int getAdjacentBugs() {
		return this.adjacentBugs;
	}
	
	public void setAdjacentBugs(int adjacentBugs) {
		this.adjacentBugs = adjacentBugs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tileType == null) ? 0 : tileType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tile other = (Tile) obj;
		if (tileType != other.tileType)
			return false;
		return true;
	}
	
	
	
}
