package year2019.day24;

import java.util.Arrays;

public class Map {
	private Tile[][] tiles;
	private int width;
	private int height;
	
	public Map(Map map) {
		this.tiles = new Tile[map.getWidth()][map.getHeight()];
		
		for (int x = 0; x < map.getWidth(); x++)
		for (int y = 0; y < map.getHeight(); y++) {
			this.tiles[x][y] = new Tile(map.getTile(x, y));
		}
		
		this.width = map.getWidth();
		this.height = map.getHeight();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + Arrays.deepHashCode(tiles);
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Map other = (Map) obj;
		if (height != other.height)
			return false;
		if (!Arrays.deepEquals(tiles, other.tiles))
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	public Tile[][] getTiles() { 
		return tiles;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public Map(int width, int height) {
		this.width = width;
		this.height = height;

		tiles = new Tile[width][height];
	}
	
	public Tile getTile(int x, int y) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
			return null;
		}
		return tiles[x][y];
	}
	
	public void setTile(Tile tile, int x, int y) {
		this.tiles[x][y] = tile;
	}

}
