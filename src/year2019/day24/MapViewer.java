package year2019.day24;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.Timer;

public class MapViewer extends JFrame implements ActionListener {
	private World world;
	
	private Timer updateTimer;
	
	private static int xOffset = 100;
	private static int yOffset = 100;
	
	private boolean updateWorldToggle;
	
	public MapViewer(World world) {
		this.setSize(400, 500);
        this.setTitle("Advent of Code - Viewer DEPTH: " + world.getDepth());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        this.world = world;
        
        this.updateWorldToggle = false;
        
        JButton nextButton = new JButton("Next");
        
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	if (!updateWorldToggle) {
            		updateWorldToggle = true;
            		world.processMinute();
            	} else {
            		updateWorldToggle = false;
            		world.updateWorld();
            	}
            }
        });
        
        this.add(nextButton);
        
        
        this.updateTimer = new Timer(10, this);
        this.updateTimer.start();
	}
	
	@Override
    public void paint(Graphics g) 
    {
		for (int x = 0; x < this.world.getWidth(); x++)
		for (int y = 0; y < this.world.getHeight(); y++) {
			Tile tile = this.world.getTileOnPosition(x, y);
			
			switch (tile.getType()) {
				case BUG:
					g.setColor(Color.RED);
					break;
				case RECURSIVE_WORLD:
					g.setColor(Color.GREEN);
					break;
				default:
					g.setColor(Color.BLACK);
					break;
			}
		
			g.fillRect(x * 20 + MapViewer.xOffset, y * 20 + MapViewer.yOffset, 20, 20);
			
			g.setColor(Color.YELLOW);
			g.drawString("" + tile.getAdjacentBugs(), x * 20 + MapViewer.xOffset + 10, y * 20 + MapViewer.yOffset + 10);
		}
    }
	
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == this.updateTimer) {
			this.repaint();
		}
	}
}
