package year2019.day25;

public enum Direction {
	NORTH,
	SOUTH,
	WEST,
	EAST
}
