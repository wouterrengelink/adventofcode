package year2019.day25;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MapPanel extends JPanel implements ActionListener {
	private Map map;
	private Timer updateTimer;
	
	private static int xOffSet = 500;
	private static int yOffSet = 500;
	
	public MapPanel(Map map) {
		this.map = map;
		
        this.updateTimer = new Timer(10, this);
        this.updateTimer.start();
	}
	
	@Override
    public void paint(Graphics g) 
    {	
		HashMap<Coordinate, Tile> bufferMap = new HashMap<Coordinate, Tile>(this.map.getMap());
		
		for(HashMap.Entry<Coordinate, Tile> entry : bufferMap.entrySet()) {
			Coordinate coordinate = entry.getKey();
			Tile tile = entry.getValue();
			
			switch (tile.getType()) {
				case CURRENT:
					g.setColor(Color.YELLOW);
					break;
				case VISITED:
					g.setColor(Color.BLACK);
					break;
				case ITEM:
					g.setColor(Color.PINK);
					break;
				case NEW:
					g.setColor(Color.GREEN);
					break;
			}
			
			g.fillRect(coordinate.getX() * 20 + MapPanel.xOffSet, coordinate.getY() * 20 + MapPanel.yOffSet, 20, 20);
			
			g.drawString("This is gona be awesome",coordinate.getX() * 20, coordinate.getY() * 20);
		}
    }
	
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == this.updateTimer) {
			this.repaint();
		}
	}
}
