package year2019.day25;

public enum TileType {
	CURRENT,
	NEW,
	VISITED,
	ITEM
}
