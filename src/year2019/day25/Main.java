package year2019.day25;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day25/input.txt";
	private static int memorySize = 12000;
	
	public static void main(String[] args) {
		System.out.println("Day 25 year 2019");

		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		Map map = new Map();
		
		Robot robot = new Robot(program, memorySize, map);
		MapViewer mapViewer = new MapViewer(map);

		int iterator = 0;
		ArrayList<String> commandList = new ArrayList<String>();
		commandList.add("east");
		commandList.add("east");
		commandList.add("east");
		commandList.add("east");
		commandList.add("west");
		commandList.add("west");
		commandList.add("west");
		commandList.add("north");
		commandList.add("take coin");
		commandList.add("east");
		commandList.add("take mutex");
		commandList.add("west");
		commandList.add("south");
		commandList.add("take ornament");
		commandList.add("east");
		commandList.add("take candy cane");
		commandList.add("west");
		commandList.add("north");
		commandList.add("south");
		commandList.add("west");
		commandList.add("north");
		commandList.add("take mug");
		commandList.add("north");
		commandList.add("take food ration");
		commandList.add("south");
		commandList.add("east");
		commandList.add("north");
		commandList.add("east");
		commandList.add("take semiconductor");
		commandList.add("east");
		commandList.add("west");
		commandList.add("west");
		commandList.add("north");
		commandList.add("south");
		commandList.add("south");
		commandList.add("west");
		commandList.add("north");
		commandList.add("south");
		commandList.add("south");
		commandList.add("south");
		commandList.add("west");
		commandList.add("north");
		commandList.add("south");
		commandList.add("east");
		commandList.add("east");
		commandList.add("take mouse");
		commandList.add("south");
		commandList.add("west");
		commandList.add("north");
		commandList.add("west");
		commandList.add("north");
		commandList.add("north");
		commandList.add("north");
		commandList.add("south");
		commandList.add("south");
		commandList.add("south");
		commandList.add("west");
		commandList.add("north");
		commandList.add("south");
		commandList.add("east");
		commandList.add("east");
		commandList.add("south");
		commandList.add("west");
		commandList.add("inv");
			
		Inventory playerInventory = new Inventory();
		Inventory groundInventory = new Inventory();
		boolean inventoryParsed = false;
		
		while (!robot.isHalted()) {
			robot.run();
			
			if (robot.requiresInput()) {
				robot.parseDescription();
				
				robot.drawPosition();
				
				if (commandList.size() > iterator) {
					robot.clearCurrentDescription();
					robot.insertCommand(commandList.get(iterator));
					iterator++;
					System.out.println(robot.getCurrentDescription());
				} else {
					robot.drawPosition();
					
					if (!inventoryParsed) {
						playerInventory.parseInventory(robot.getCurrentDescription());
						inventoryParsed = true;
					}

					if (StringParser.containersWord(robot.getCurrentDescription(), "Droids on this ship are lighter than the detected value")) {
						Item itemToDrop = playerInventory.getRandomItem();
						commandList.add("drop " + itemToDrop.getName());
						groundInventory.addItem(itemToDrop);
						System.out.println("Dropping " + itemToDrop.getName());
					} else if (StringParser.containersWord(robot.getCurrentDescription(), "Droids on this ship are heavier than the detected value")) {
						Item itemToPickup = groundInventory.getRandomItem();
						commandList.add("take " + itemToPickup.getName());
						playerInventory.addItem(itemToPickup);
						System.out.println("Picking up " + itemToPickup.getName());
					}
					
					commandList.add("west");
					
					System.out.println(robot.getCurrentDescription());
					
					robot.ejectBack();
				}
			}
		}
		System.out.println(robot.getCurrentDescription());
		System.out.println("Halted");
	}

}


