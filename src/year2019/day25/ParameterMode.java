package year2019.day25;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
