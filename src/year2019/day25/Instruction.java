package year2019.day25;

public class Instruction {
	private int opCode;
	private ParameterMode firstParameterMode;
	private ParameterMode secondParameterMode;
	private ParameterMode thirdParameterMode;
	
	public Instruction(int opCode, ParameterMode firstParameterMode, ParameterMode secondParameterMode, ParameterMode thirdParameterMode) {
		this.opCode = opCode;
		this.firstParameterMode = firstParameterMode;
		this.secondParameterMode  = secondParameterMode;
		this.thirdParameterMode = thirdParameterMode;
	}
	
	public int getOpCode() {
		return this.opCode;
	}
	
	public ParameterMode getFirstParameterMode() {
		return this.firstParameterMode;
	}
	
	public ParameterMode getSecondParameterMode() {
		return this.secondParameterMode;
	}
	
	public ParameterMode getThirdParameterMode() {
		return this.thirdParameterMode;
	}
}
