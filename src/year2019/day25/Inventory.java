package year2019.day25;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Inventory {
	private ArrayList<Item> itemList;
	
	public Inventory() {
		this.itemList = new ArrayList<Item>();
	}
	
	public void addItem(Item item) {
		this.itemList.add(item);
	}
	
	public Item getRandomItem() {
		Random random = new Random();
		int randomIndex = random.nextInt(this.itemList.size());
		
		Item selectedItem = this.itemList.get(randomIndex); 
		
		this.itemList.remove(randomIndex);
		
        return selectedItem;
	}
	
	public void parseInventory(String inventoryString) {
		this.itemList = new ArrayList<Item>();
		
		String regex = "- ([a-z ]+)";
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(inventoryString);
		
		 while (matcher.find()) {    
             this.itemList.add(new Item(matcher.group(1)));
         }    
	}
}
