package year2019.day25;

import java.math.BigInteger;
import java.util.ArrayList;

public class ParameterModeParser {
	private BigInteger relativeBase;
	
	public ParameterModeParser() {
		this.relativeBase = new BigInteger("0");
	}
	
	public static ParameterMode parseParameterMode(int parameterMode) {
		switch (parameterMode) {
			case 0:
				return ParameterMode.POSITION;
			case 1:
				return ParameterMode.IMMEDIATE;
			case 2:
				return ParameterMode.RELATIVE;
		}
		
		return null;
	}
	
	public BigInteger getValue(ArrayList<BigInteger> program, ParameterMode parameterMode, BigInteger programValue) {
		BigInteger value = new BigInteger("-1");
		
		if (parameterMode == ParameterMode.POSITION) {
			value = program.get(programValue.intValue());
		} else if (parameterMode == ParameterMode.IMMEDIATE) {
			value = programValue;
		} else if (parameterMode == ParameterMode.RELATIVE) {
			value = this.getRelativeValue(program, programValue);
		}
		
		return value;
	}
	
	// this was hacked together, errors could be in this method
	public BigInteger getInputValue(ArrayList<BigInteger> program, ParameterMode parameterMode, BigInteger programValue) {
		BigInteger value = new BigInteger("-1");
		
		if (parameterMode == ParameterMode.POSITION) {
			value = programValue;
		} else if (parameterMode == ParameterMode.IMMEDIATE) {
			value = programValue;
		} else if (parameterMode == ParameterMode.RELATIVE) {
			value = this.relativeBase.add(programValue);
		}
		
		return value;
	}
	
	private BigInteger getRelativeValue(ArrayList<BigInteger> program, BigInteger programValue) {
		int relativePosition = this.relativeBase.add(programValue).intValue();
		
		return program.get(relativePosition);
	}
	
	public void setRelativeBase(BigInteger relativeBase) {
		this.relativeBase = relativeBase;
	}
	
	public BigInteger getRelativeBase() {
		return this.relativeBase;
	}
}
