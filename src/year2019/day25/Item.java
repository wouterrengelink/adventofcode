package year2019.day25;

public class Item {
	private String name;
	
	public Item(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
