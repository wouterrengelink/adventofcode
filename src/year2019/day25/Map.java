package year2019.day25;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import general.StringParser;

public class Map {
	private HashMap<Coordinate, Tile> map;
	private HashMap<Coordinate, Boolean> visitedMap;
	private HashMap<Coordinate, Boolean> itemMap;
	private Coordinate currentPosition;
	
	public Map() {
		this.map = new HashMap<Coordinate, Tile>();
		this.visitedMap = new HashMap<Coordinate, Boolean>();
		this.itemMap = new HashMap<Coordinate, Boolean>();
	}
	
	public void setCurrentPosition(Coordinate position) {
		this.currentPosition = new Coordinate(position);
		
		this.visitedMap.put(new Coordinate(position), true);
		this.map.put(position, new Tile(TileType.CURRENT));
	}
	
	public HashMap<Coordinate, Tile> getMap() {
		return this.map;
	}
	
	public void parseDescription(String string) {
		if (StringParser.containersWord(string, "Items here")) {
			this.itemMap.put(new Coordinate(this.currentPosition), true);
		}
		
		if (StringParser.containersWord(string, "You take the")) {
			this.itemMap.remove(this.currentPosition);
		}
		
		if (StringParser.containersWord(string, "north")) {
			this.discoverTile(Direction.NORTH);
		}
		
		if (StringParser.containersWord(string, "south")) {
			this.discoverTile(Direction.SOUTH);
		}
		
		if (StringParser.containersWord(string, "east")) {
			this.discoverTile(Direction.EAST);
		}
		
		if (StringParser.containersWord(string, "west")) {
			this.discoverTile(Direction.WEST);
		}
	}
	
	private void discoverTile(Direction direction) {
		Coordinate coordinate = null;
		
		switch (direction) {
			case NORTH:
				coordinate = new Coordinate(this.currentPosition);
				coordinate.setY(coordinate.getY() - 1);
				this.addRoom(coordinate);
				break;
			case SOUTH:
				coordinate = new Coordinate(this.currentPosition);
				coordinate.setY(coordinate.getY() + 1);
				this.addRoom(coordinate);
				break;
			case EAST:
				coordinate = new Coordinate(this.currentPosition);
				coordinate.setX(coordinate.getX() + 1);
				this.addRoom(coordinate);
				break;
			case WEST:
				coordinate = new Coordinate(this.currentPosition);
				coordinate.setX(coordinate.getX() - 1);
				this.addRoom(coordinate);
				break;
		}
	}
	
	private void addRoom(Coordinate coordinate) {
		if (coordinate.equals(this.currentPosition)) {
			this.map.put(coordinate, new Tile(TileType.CURRENT));
		} else if (this.itemMap.containsKey(coordinate)) {
			this.map.put(coordinate, new Tile(TileType.ITEM));
		} else if (this.visitedMap.containsKey(coordinate)) {
			this.map.put(coordinate, new Tile(TileType.VISITED));
		} else {
			this.map.put(coordinate, new Tile(TileType.NEW));
		}
	}
	
	public void clearCurrentPosition(Coordinate coordinate) {
		// this.map.put(coordinate, new Tile(TileType.VISITED));
	}
	
}
