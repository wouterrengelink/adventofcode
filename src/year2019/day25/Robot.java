package year2019.day25;

import java.math.BigInteger;
import java.util.ArrayList;

public class Robot {
	private Computer computer;
	private Coordinate position;
	private String currentDescription;
	private Map map;
	
	public Robot(ArrayList<BigInteger> program, int memorySize, Map map) {
		this.computer = new Computer(program, memorySize);
		this.position = new Coordinate(0, 0);
		this.map = map;
		this.clearCurrentDescription();
		
		this.map.setCurrentPosition(this.position);
	}
	
	public void ejectBack() {
		this.position.setX(this.position.getX() + 1);
	}
	
	public void run() {
		BigInteger output = this.computer.run();
		if (output != null) {
			this.currentDescription += (char) output.intValue();
//			System.out.print("" + (char) output.intValue());
		}
		this.map.setCurrentPosition(this.position);
	}
	
	public void reset() {
		this.computer.reset();
	}
	
	private void moveRobot(String string) {
		switch (string) {
			case "north":
				this.map.clearCurrentPosition(this.position);
				this.position.setY(this.position.getY() - 1);
				this.map.setCurrentPosition(this.position);
				break;
			case "south":
				this.map.clearCurrentPosition(this.position);
				this.position.setY(this.position.getY() + 1);
				this.map.setCurrentPosition(this.position);
				break;
			case "east":
				this.map.clearCurrentPosition(this.position);
				this.position.setX(this.position.getX() + 1);
				this.map.setCurrentPosition(this.position);
				break;
			case "west":
				this.map.clearCurrentPosition(this.position);
				this.position.setX(this.position.getX() - 1);
				this.map.setCurrentPosition(this.position);
				break;
		}
	}
	
	public void parseDescription() {
		this.map.parseDescription(this.currentDescription);
	}
	
	public void insertCommand(String string) {
		this.moveRobot(string); // it's very important to move robot after map discovery
		
		for (char character : string.toCharArray()) {
			this.computer.addInput(new BigInteger("" + (int) character));
		}
		
		this.computer.addInput(new BigInteger("" + 10)); // \n
	}
	
	public void clearCurrentDescription() {
		this.currentDescription = "";
	}
	
	public String getCurrentDescription() {
		return this.currentDescription;
	}
	
	public boolean isHalted() {
		return this.computer.isHalted();
	}
	
	public boolean requiresInput() {
		return this.computer.requiresInput();
	}
	
	public void drawPosition() {
		this.map.getMap().put(this.position, new Tile(TileType.CURRENT));
	}
	
	public void printPosition() {
		System.out.println("x: " + this.position.getX() + " y: " + this.position.getY());
	}
}
