package year2019.day15;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
