package year2019.day15;

public enum TileType {
	EMPTY,
	WALL,
	OXYGEN_SYSTEM
}
