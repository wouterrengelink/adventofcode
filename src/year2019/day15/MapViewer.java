package year2019.day15;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.Timer;

public class MapViewer extends JFrame implements ActionListener {
	private HashMap<Coordinate, Tile> map;
	private Coordinate droidPosition;
	private Coordinate startingPosition;
	private Timer updateTimer;
	
	private static int xOffSet = 600;
	private static int yOffSet = 400;
	
	public MapViewer(HashMap<Coordinate, Tile> map, Coordinate droidPosition) {
		this.map = map;
		this.droidPosition = droidPosition;
		this.startingPosition = new Coordinate(droidPosition);
		
		this.setSize(1200, 1000);
        this.setTitle("Advent of Code - Viewer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        this.updateTimer = new Timer(10, this);
        this.updateTimer.start();
	}
	
	@Override
    public void paint(Graphics g) 
    {
		for(HashMap.Entry<Coordinate, Tile> entry : this.map.entrySet()) {
			Coordinate coordinate = entry.getKey();
			Tile tile = entry.getValue();
			
			switch (tile.getType()) {
				case WALL:
					g.setColor(Color.BLACK);
					break;
				case EMPTY:
					g.setColor(Color.WHITE);
					break;
				case OXYGEN_SYSTEM:
					g.setColor(Color.BLUE);
					break;
			}
			
			g.fillRect(coordinate.getX() * 20 + MapViewer.xOffSet, coordinate.getY() * 20 + MapViewer.yOffSet, 20, 20);
		}
		
		// draw droid position
		this.drawDroidPosition(g);
		this.drawStartingPosition(g);
    }
	
	private void drawStartingPosition(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillOval(this.startingPosition.getX() * 20 + MapViewer.xOffSet, this.startingPosition.getY() * 20 + MapViewer.yOffSet, 20, 20);
	}
		
	private void drawDroidPosition(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval(this.droidPosition.getX() * 20 + MapViewer.xOffSet, this.droidPosition.getY() * 20 + MapViewer.yOffSet, 20, 20);
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == this.updateTimer) {
			this.repaint();
		}
	}
}
