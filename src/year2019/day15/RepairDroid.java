package year2019.day15;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class RepairDroid {
	private Computer computer;
	private HashMap<Coordinate, Tile> map;
	private HashMap<Coordinate, Boolean> visitedMap;
	private Coordinate position;
	private Coordinate previousPosition;
	private Coordinate oxygenSystemPosition;
	private Coordinate startingPosition;
	
	private int numberOfSteps;
	
	public RepairDroid(ArrayList<BigInteger> program, int memorySize) {
		this.computer = new Computer(program, memorySize);
		this.map = new HashMap<Coordinate, Tile>();
		this.visitedMap = new HashMap<Coordinate, Boolean>();
		this.position = new Coordinate(0, 0);
		this.previousPosition = new Coordinate(0, 0);
		this.map.put(new Coordinate(0,0), new Tile(TileType.EMPTY));
		this.numberOfSteps = 0;
		this.startingPosition = new Coordinate(0, 0);
	}
	
	public int run() {
		int statusCode = -1;
		
		BigInteger movementDirection = this.getMovementDirection();
		this.computer.addInput(movementDirection);
		
		statusCode = this.computer.run().intValue();

		switch (statusCode) {
			case 0:
				// The repair droid hit a wall. Its position has not changed
				this.addToMap(movementDirection.intValue(), TileType.WALL);
				break;
			case 1:
				// The repair droid has moved one step in the requested direction
				this.addToMap(movementDirection.intValue(), TileType.EMPTY);
				this.previousPosition = new Coordinate(this.position);
				this.moveInDirection(movementDirection.intValue());
				break;
			case 2:
				// The repair droid has moved one step in the requested direction; its new position is the location of the oxygen system
				this.addToMap(movementDirection.intValue(), TileType.OXYGEN_SYSTEM);
				this.previousPosition = new Coordinate(this.position);
				this.oxygenSystemPosition = new Coordinate(this.position);
				this.moveInDirection(movementDirection.intValue());
				break;
		}
		
		this.visitedMap.put(this.position, new Boolean(true));
		
		return statusCode;
	}
	
	private void addToMap(int movementDirection, TileType tileType) {
		switch (movementDirection) {
			case 1: // north
				this.map.put(new Coordinate(this.position.getX(), this.position.getY() - 1), new Tile(tileType));
				break;
			case 2: // south
				this.map.put(new Coordinate(this.position.getX(), this.position.getY() + 1), new Tile(tileType));
				break;
			case 3: // west
				this.map.put(new Coordinate(this.position.getX() - 1, this.position.getY()), new Tile(tileType));
				break;
			case 4: // east
				this.map.put(new Coordinate(this.position.getX() + 1, this.position.getY()), new Tile(tileType));
				break;
			default:
				System.out.println("Something has happened which shouln't");
		}
	}
	
	private void moveInDirection(int movementDirection) {
		switch (movementDirection) {
			case 1: // north
				this.position.setY(this.position.getY() - 1);
				break;
			case 2: // south
				this.position.setY(this.position.getY() + 1);
				break;
			case 3: // west
				this.position.setX(this.position.getX() - 1);
				break;
			case 4: // east
				this.position.setX(this.position.getX() + 1);
				break;
			default:
				System.out.println("Something has happened which shouln't");
		}
		
		this.numberOfSteps++;
	}
	
	private BigInteger getMovementDirection() {
//		// try south
//		boolean isPreviousPosition = false;
//		
//		Coordinate southCoordinate = new Coordinate(this.position.getX(), this.position.getY() + 1);
//		Tile southTile = this.getTileOnPosition(southCoordinate);
//		isPreviousPosition = this.previousPosition.equals(southCoordinate);
//		if (!isPreviousPosition && (southTile == null || southTile.getType() != TileType.WALL)) {
//			return new BigInteger("2");
//		}
//				
//		// try north
//		Coordinate northCoordinate = new Coordinate(this.position.getX(), this.position.getY() - 1);
//		Tile northTile = this.getTileOnPosition(northCoordinate);
//		isPreviousPosition = this.previousPosition.equals(northCoordinate);
//		if (!isPreviousPosition && (northTile == null || northTile.getType() != TileType.WALL)) {
//			return new BigInteger("1");
//		}	
//		
//		// try west
//		Coordinate westCoordinate = new Coordinate(this.position.getX() - 1, this.position.getY());
//		Tile westTile = this.getTileOnPosition(westCoordinate);
//		isPreviousPosition = this.previousPosition.equals(westCoordinate);
//		if (!isPreviousPosition && (westTile == null || westTile.getType() != TileType.WALL)) {
//			return new BigInteger("3");
//		}
//		
//		// try east
//		Coordinate eastCoordinate = new Coordinate(this.position.getX() + 1, this.position.getY());
//		Tile eastTile = this.getTileOnPosition(eastCoordinate);
//		isPreviousPosition = this.previousPosition.equals(eastCoordinate);
//		if (!isPreviousPosition && (eastTile == null || eastTile.getType() != TileType.WALL)) {
//			return new BigInteger("4");
//		}
		
		return this.getRandomDirection();
	}
	
	private BigInteger getRandomDirection() {
		Random random = new Random();
		int low = 1;
		int high = 5;
		int result = random.nextInt(high - low) + low;
		
		return new BigInteger("" + result);
	}
	
	private Tile getTileOnPosition(Coordinate coordinate) {
		return this.map.get(coordinate);
	}
	
	public HashMap<Coordinate, Tile> getMap() {
		return this.map;
	}
	
	public Coordinate getStartingPosition() {
		return this.startingPosition;
	}
	
	public Coordinate getPosition() {
		return this.position;
	}
	
	public Coordinate getOxygenSystemPosition() {
		return this.oxygenSystemPosition;
	}
	
	public int getNumberOfSteps() {
		return this.numberOfSteps;
	}
	
	public void reset() {
		this.computer.reset();
	}
}
