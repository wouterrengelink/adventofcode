package year2019.day15;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day15/input.txt";
	private static int memorySize = 4096;
	
	public static void main(String[] args) {
		System.out.println("Day 15 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		RepairDroid repairDroid = new RepairDroid(program, memorySize);
		MapViewer mapViewer = new MapViewer(repairDroid.getMap(), repairDroid.getPosition());
		
		while(repairDroid.run() != 2) {}

		Coordinate startingPosition = repairDroid.getStartingPosition();
		Coordinate oxigenSystemPosition = repairDroid.getOxygenSystemPosition();
		HashMap<Coordinate, Tile> map = repairDroid.getMap();
		
		System.out.println("Oxygen System found");
		System.out.println("X: " + oxigenSystemPosition.getX() + " Y:" + oxigenSystemPosition.getY());
		
		MapAnalyser mapAnalyser = new MapAnalyser(map);
		
		int minimumSteps = mapAnalyser.getMinimumSteps();
		
		System.out.println("Minimum number of steps: " + minimumSteps);
		
		System.out.println("Done");
	}

}
