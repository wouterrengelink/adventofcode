package year2019.day2;

import java.util.ArrayList;

public class ProgramParser {
	private ArrayList<String> program;
	private ArrayList<String> initialProgram;
	private int position;
	
	public ProgramParser(ArrayList<String> program) {
		this.initialProgram = program;
		this.reset();
	}
	
	public void reset() {
		this.program = new ArrayList<String>(this.initialProgram);
		this.position = 0;
	}
	
	public void run() {
		boolean programHalted = false;
		
		while (!programHalted) {
			String value = this.program.get(this.position);
			
			if (value.equals("1")) {
				this.add();
				this.position += 4;
			} else if (value.equals("2")) {
				this.multiply();
				this.position += 4;
			} else {
				// 99
				programHalted = true;
				this.position += 1;
			}
		}
	}
	
	public void setValues(int noun, int verb) {
		this.program.set(1, "" + noun);
		this.program.set(2, "" + verb);
	}
	
	private void multiply() {
		int firstPosition = Integer.parseInt(this.program.get(this.position + 1));
		int secondPosition = Integer.parseInt(this.program.get(this.position + 2));
		int resultPosition = Integer.parseInt(this.program.get(this.position + 3));
		
		int firstValue = Integer.parseInt(this.program.get(firstPosition));
		int secondValue = Integer.parseInt(this.program.get(secondPosition));
		
		int addedValue = firstValue * secondValue;
		
		this.program.set(resultPosition, "" + addedValue);
	}
	
	private void add() {
		int firstPosition = Integer.parseInt(this.program.get(this.position + 1));
		int secondPosition = Integer.parseInt(this.program.get(this.position + 2));
		int resultPosition = Integer.parseInt(this.program.get(this.position + 3));
		
		int firstValue = Integer.parseInt(this.program.get(firstPosition));
		int secondValue = Integer.parseInt(this.program.get(secondPosition));
		
		int addedValue = firstValue + secondValue;
		
		this.program.set(resultPosition, "" + addedValue);
	}
	
	public String getValueAtIndex(int index) {
		return this.program.get(index);
	}
	
	public String getValueAtZero() {
		return this.program.get(0);
	}
}
