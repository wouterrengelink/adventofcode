package year2019.day2;

import java.util.ArrayList;

import general.InputReader;

public class Main {
	
	private static String inputFile = "./src/year2019/day2/input.txt";

	public static void main(String[] args) {
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> program = inputReader.getInputFromSeparator(",");
		
		ProgramParser programParser = new ProgramParser(program);
		
		//before running the program, replace position 1 with the value 12 and replace position 2 with the value 2
		for (int i=0; i<=99; i++) {
			for (int j=0; j<=99; j++) {
				programParser.setValues(i, j);

				programParser.run();
				
				if (programParser.getValueAtZero().equals("19690720")) {
					System.out.println("first " + i + " second " + j);
					
					int answer = 100 * i + j;
					
					System.out.println("answer " + answer);
				}
				
				// reset computer
				programParser.reset();
			}
		}	
	}

}
