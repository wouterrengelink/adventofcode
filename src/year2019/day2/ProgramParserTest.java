package year2019.day2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;

class ProgramParserTest {

	@Test
	void addTest() {
		ArrayList<String> program = new ArrayList<String>();
		program.add("1");
		program.add("0");
		program.add("0");
		program.add("0");
		program.add("99");
		
		ProgramParser programParser = new ProgramParser(program);
		
		programParser.run();
		
		if (!programParser.getValueAtIndex(0).equals("2")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(1).equals("0")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(2).equals("0")) {
			fail("Test failed");
		}

		if (!programParser.getValueAtIndex(3).equals("0")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(4).equals("99")) {
			fail("Test failed");
		}
	}
	
	@Test
	void multiplyTest() {
		ArrayList<String> program = new ArrayList<String>();
		program.add("2");
		program.add("3");
		program.add("0");
		program.add("3");
		program.add("99");
		
		ProgramParser programParser = new ProgramParser(program);
		
		programParser.run();
		
		if (!programParser.getValueAtIndex(0).equals("2")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(1).equals("3")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(2).equals("0")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(3).equals("6")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(4).equals("99")) {
			fail("Test failed");
		}
	}
	
	@Test
	void anotherTest() {
		ArrayList<String> program = new ArrayList<String>();
		program.add("2");
		program.add("4");
		program.add("4");
		program.add("5");
		program.add("99");
		program.add("0");
		
		ProgramParser programParser = new ProgramParser(program);
		
		programParser.run();
		
		if (!programParser.getValueAtIndex(0).equals("2")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(1).equals("4")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(2).equals("4")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(3).equals("5")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(4).equals("99")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(5).equals("9801")) {
			fail("Test failed");
		}
	}
	
	@Test
	void anotherTest2() {
		ArrayList<String> program = new ArrayList<String>();
		program.add("1");
		program.add("1");
		program.add("1");
		program.add("4");
		program.add("99");
		program.add("5");
		program.add("6");
		program.add("0");
		program.add("99");
				
		ProgramParser programParser = new ProgramParser(program);
		
		programParser.run();
		
		if (!programParser.getValueAtIndex(0).equals("30")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(1).equals("1")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(2).equals("1")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(3).equals("4")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(4).equals("2")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(5).equals("5")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(6).equals("6")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(7).equals("0")) {
			fail("Test failed");
		}
		
		if (!programParser.getValueAtIndex(8).equals("99")) {
			fail("Test failed");
		}
	}

}
