package year2019.day14;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;

import general.InputReader;

public class Main {
	private static String inputFile = "./src/year2019/day14/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Day 14 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		BigInteger numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1);
		
		System.out.println(numberOfOreForFuel);
		
		System.out.println("Done");
	}

}
