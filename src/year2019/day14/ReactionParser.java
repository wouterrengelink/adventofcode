package year2019.day14;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReactionParser {	
	private HashSet<Chemical> chemicalSet;
	
	public ReactionParser(HashSet<Chemical> chemicalSet) {
		this.chemicalSet = chemicalSet;
	}
	
	public ArrayList<Reaction> getReactions(ArrayList<String> input) {
		ArrayList<Reaction> reactionList = new ArrayList<Reaction>();
		
		String regex = "([0-9]+) ([A-Z]+)";
		Pattern pattern = Pattern.compile(regex);
		
		for(String reactionString : input) {
			Matcher matcher = pattern.matcher(reactionString);

			ArrayList<String> matches = new ArrayList<String>();
			
			while (matcher.find()) {
				matches.add(matcher.group(0));
			}
			
			// reverse arraylist to have solution at position 0
			Collections.reverse(matches);

			Reaction reaction = this.parseMatches(matches);
			
			reactionList.add(reaction);
		}
		
		return reactionList;
	}
	
	private Reaction parseMatches(ArrayList<String> matches) {
		Reaction reaction = new Reaction();
		
		boolean solutionFound = false;
		String regex = "([0-9]+) ([A-Z]+)";
		Pattern pattern = Pattern.compile(regex);
		
		for (String match : matches) {
			Matcher matcher = pattern.matcher(match);
			
			matcher.find();
			
			if (!solutionFound) {
				reaction.addSolution(new Chemical(matcher.group(2)), new BigInteger("" + Integer.parseInt(matcher.group(1))));
				solutionFound = true;
			} else {
				reaction.addInputChemical(new Chemical(matcher.group(2)), new BigInteger("" + Integer.parseInt(matcher.group(1))));
			}
		}
		
		return reaction;
	}

}
