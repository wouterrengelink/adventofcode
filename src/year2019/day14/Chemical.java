package year2019.day14;

import year2019.day13.Coordinate;

public class Chemical {
	private String name;
	
	public Chemical(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object object) { 
		if (object == this) { 
		    return true; 
		} 
	  
	    if (!(object instanceof Chemical)) { 
	        return false; 
	    } 
	      
	    Chemical chemical = (Chemical) object; 
	      
	    if (this.name.equals(chemical.getName())) {
	    	return true;
	    }
	    
	    return false;
	} 
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	public String getName() {
		return this.name;
	}

}
