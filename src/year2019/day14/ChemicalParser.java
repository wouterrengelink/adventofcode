package year2019.day14;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChemicalParser {

	public static HashSet<Chemical> getChemicals(ArrayList<String> input) {
		HashSet<Chemical> chemicalSet = new HashSet<Chemical>();
		
		String regex = "([A-Z]+)";
		Pattern pattern = Pattern.compile(regex);
		
		for(String reactionString : input) {
			Matcher matcher = pattern.matcher(reactionString);
			
			while (matcher.find()) {
				chemicalSet.add(new Chemical(matcher.group(0)));
			}

		}
		
		return chemicalSet;
	}
}
