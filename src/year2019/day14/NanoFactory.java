package year2019.day14;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class NanoFactory {
	private ArrayList<String> input;
	private HashSet<Chemical> chemicalSet;
	private ArrayList<Reaction> reactionList;
	private HashMap<Chemical, BigInteger> inventory;
	private BigInteger numberOfOre;
	
	public NanoFactory(ArrayList<String> input) {
		this.input = input;
		this.chemicalSet = ChemicalParser.getChemicals(input);

		ReactionParser reactionParser = new ReactionParser(chemicalSet);
		
		this.reactionList = reactionParser.getReactions(input);
		
		this.inventory = new HashMap<Chemical, BigInteger>();
		
		this.numberOfOre = new BigInteger("" + 0);
	}
	
	public int numberOfFuelForOre(BigInteger oreAmount) {
		int fuelCounter = 1;
		BigInteger numberOfOreForFuel = this.numberOfOreForFuel(1);
		
		while (oreAmount.compareTo(numberOfOreForFuel) == 1) {
			
			oreAmount = oreAmount.subtract(numberOfOreForFuel);
			fuelCounter++;
		}
		
		return fuelCounter;
	}
	
	public BigInteger numberOfOreForFuel(int amountOfFuel) {
		for (Chemical chemical : this.chemicalSet) {
			this.inventory.put(chemical, new BigInteger("0"));
		}

		this.getChemicalFromInventory(new Chemical("FUEL"), new BigInteger("" + amountOfFuel));
		
		return this.numberOfOre;
	}
	
	private void getChemicalFromInventory(Chemical chemical, BigInteger amount) {
		BigInteger amountInInventory = this.inventory.get(chemical);
		BigInteger amountCreated = new BigInteger("" + 0);
		
		// if not enough in inventory create product unit there is enough
		while (amountInInventory.compareTo(amount) == -1) {
			amountCreated = amountCreated.add(this.createProduct(chemical));
			amountInInventory = this.inventory.get(chemical);
		}

		
		// remove number of chemicals stored in inventory
		this.inventory.put(chemical, amountInInventory.subtract(amount));
	}
	
	private BigInteger createProduct(Chemical chemical) {
		Reaction reaction = this.getReactionWithSolution(chemical);
		BigInteger solutionAmount = reaction.getAmountOfSolution();
		HashMap<Chemical, BigInteger> ingredients = reaction.getIngredients();
		
		BigInteger numberOfOre = ingredients.get(new Chemical("ORE"));
		
		if (numberOfOre != null) {
			this.numberOfOre = this.numberOfOre.add(new BigInteger("" + numberOfOre));
		} else {
			for (HashMap.Entry<Chemical, BigInteger> entry : ingredients.entrySet()) {
				Chemical entryChemical = entry.getKey();
				BigInteger amount = entry.getValue();
				
				this.getChemicalFromInventory(entryChemical, amount);
			}
		}
		
		BigInteger amountInInventory = this.inventory.get(chemical);
		
		this.inventory.put(chemical, amountInInventory.add(solutionAmount));
		
		return solutionAmount;
	}

	private Reaction getReactionWithSolution(Chemical chemical) {
		for (Reaction reaction : this.reactionList) {
			Chemical solution = reaction.getSolution();
			
			if (solution.equals(chemical)) {
				return reaction;
			}
		}
		
		return null;
	}
}
