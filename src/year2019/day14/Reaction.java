package year2019.day14;

import java.math.BigInteger;
import java.util.HashMap;

public class Reaction {
	private HashMap<Chemical, BigInteger> reactionMap;
	private HashMap<Chemical, BigInteger> solutionMap;
	
	public Reaction() {
		this.reactionMap = new HashMap<Chemical, BigInteger>();
		this.solutionMap = new HashMap<Chemical, BigInteger>();
	}
	
	public void addInputChemical(Chemical chemical, BigInteger amount) {
		this.reactionMap.put(chemical, amount);
	}
	
	public void addSolution(Chemical chemical, BigInteger amount) {
		this.solutionMap.put(chemical, amount);
	}
	
	public BigInteger getAmountOfSolution() {
		for (HashMap.Entry<Chemical, BigInteger> entry : this.solutionMap.entrySet()) {
			return entry.getValue();
		}
		
		return null;
	}
	
	public Chemical getSolution() {
		for (HashMap.Entry<Chemical, BigInteger> entry : this.solutionMap.entrySet()) {
			return entry.getKey();
		}
		
		return null;
	}
	
	public HashMap<Chemical, BigInteger> getIngredients() {
		return this.reactionMap;
	}

}
