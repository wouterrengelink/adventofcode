package year2019.day14;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import general.InputReader;

class NanoFactoryTest {

	@Test
	void simplestTest() {
		String inputFile = "./src/year2019/day14/simplestTest.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		int numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1).intValue();
		
		if (numberOfOreForFuel != 31) {
			fail("Ore for fuel not 31 but " + numberOfOreForFuel);
		}
		
	}
	
	@Test
	void shortTest() {
		String inputFile = "./src/year2019/day14/shortTest.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		int numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1).intValue();
		
		if (numberOfOreForFuel != 165) {
			fail("Ore for fuel not 165 but " + numberOfOreForFuel);
		}
		
	}
	
	@Test
	void test13312() {
		String inputFile = "./src/year2019/day14/test13312.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		int numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1).intValue();
		
		if (numberOfOreForFuel != 13312) {
			fail("Ore for fuel not 13312 but " + numberOfOreForFuel);
		}
		
		int numberOfFuelPerOre = nanoFactory.numberOfFuelForOre(new BigInteger("1000000000000"));
	
		if (numberOfFuelPerOre != 82892753) {
			fail("Fuel for ore not 82892753 but " + numberOfFuelPerOre);
		}
	}
	
	@Test
	void test180697() {
		String inputFile = "./src/year2019/day14/test180697.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		int numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1).intValue();
		
		if (numberOfOreForFuel != 180697) {
			fail("Ore for fuel not 180697 but " + numberOfOreForFuel);
		}
		
		int numberOfFuelPerOre = nanoFactory.numberOfFuelForOre(new BigInteger("1000000000000"));
		
		if (numberOfFuelPerOre != 5586022) {
			fail("Fuel for ore not 5586022 but " + numberOfFuelPerOre);
		}
	}
	
	@Test
	void test2210736() {
		String inputFile = "./src/year2019/day14/test2210736.txt";
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> input = inputReader.getInputFromList();

		NanoFactory nanoFactory = new NanoFactory(input);
		
		int numberOfOreForFuel = nanoFactory.numberOfOreForFuel(1).intValue();
		
		if (numberOfOreForFuel != 2210736) {
			fail("Ore for fuel not 2210736 but " + numberOfOreForFuel);
		}
		
		int numberOfFuelPerOre = nanoFactory.numberOfFuelForOre(new BigInteger("1000000000000"));
		
		if (numberOfFuelPerOre != 460664) {
			fail("Fuel for ore not 460664 but " + numberOfFuelPerOre);
		}
	}

}
