package year2019.day23;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day23/input.txt";
	private static int memorySize = 18096;
	
	public static void main(String[] args) {
		System.out.println("Day 23 2019");

		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		Network network = new Network();
		
		for (int i = 0; i < 50; i++) {
			network.addDevice(new NetworkInterface(network, program, memorySize, i));
		}
		
		while (true) {
			network.run();
		}
	}

}
