package year2019.day23;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
