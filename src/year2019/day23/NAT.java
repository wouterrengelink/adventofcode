package year2019.day23;

import java.math.BigInteger;
import java.util.ArrayList;

public class NAT {
	private Packet packet;
	private Network network;
	private BigInteger lastY;
	
	public NAT(Network network) {
		this.network = network;
		this.lastY = new BigInteger("-999999");
	}
	
	public void setPacket(Packet packet) {
		this.packet = packet;
	}
	
	public void run() {
		if (this.network.isNetworkIdle()) {
			if (this.lastY.equals(this.packet.getY())) {
				System.out.println("This " + this.packet.getY());
				System.exit(0);
			}
			
			this.lastY = this.packet.getY();
			Packet packet = new Packet(0, this.packet.getX(), this.packet.getY());
			this.network.addPacket(packet);
		}
	}
}
