package year2019.day23;

import java.math.BigInteger;
import java.util.ArrayList;

public class NetworkInterface {
	private Computer computer;
	private ArrayList<Packet> packetQueue;
	private Network network;
	private int address;
	
	public NetworkInterface(Network network, ArrayList<BigInteger> program, int memorySize, int address) {
		this.computer = new Computer(program, memorySize);
		this.packetQueue = new ArrayList<Packet>();
		this.network = network;
		
		this.address = address;
		this.computer.addInput(new BigInteger("" + address));
	}

	public void run() {
//		System.out.println("Running network interface with address " + this.address);
		if (this.packetQueue.size() == 0) {
			//System.out.println("No packets in queue insert -1");
			this.computer.addInput(-1);
		} else {
			// System.out.println("Using first packet in queue");
			Packet packet = this.packetQueue.get(0);
			this.computer.addInput(packet.getX());
			this.computer.addInput(packet.getY());
			
			this.packetQueue.remove(0);
		}
		
		int address = this.computer.run().intValue();
		
		while(address != -999) {			
			BigInteger x = this.computer.run();
			BigInteger y = this.computer.run();
			
			Packet packet = new Packet(address, x, y);
			System.out.println("Sending new packet to address " + address + " with x " + x + " and y " + y);
			this.network.addPacket(packet);			
			
			address = this.computer.run().intValue();
		}
	}

	public int getAddress() {
		return this.address;
	}
	
	public void addPacket(Packet packet) {
		this.packetQueue.add(packet);
	}
	
	public ArrayList<Packet> getPacketQueue() {
		return this.packetQueue;
	}
}
