package year2019.day23;

import java.math.BigInteger;

public class Packet {
	private int address;
	private BigInteger x;
	private BigInteger y;
	
	public Packet(int address, BigInteger x, BigInteger y) {
		this.address = address;
		this.x = x;
		this.y = y;
	}
	
	public int getAddress() {
		return this.address;
	}
	
	public BigInteger getX() {
		return this.x;
	}
	
	public BigInteger getY() {
		return this.y;
	}
}
