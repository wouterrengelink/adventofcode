package year2019.day23;

import java.util.ArrayList;

public class Network {
	private ArrayList<NetworkInterface> network;
	private NAT nat;
	
	public Network() {
		this.network = new ArrayList<NetworkInterface>();
		this.nat = new NAT(this);
	}
	
	public void addDevice(NetworkInterface networkInterface) {
		this.network.add(networkInterface);
	}
	
	public void addPacket(Packet packet) {
		if (packet.getAddress() == 255) {
			System.out.println("Routing packet to NAT");
			this.nat.setPacket(packet);
		} else {
			for (NetworkInterface networkInterface : this.network) {
				if (networkInterface.getAddress() == packet.getAddress()) {
					networkInterface.addPacket(packet);
				}
			}
		}
	}
	
	public boolean isNetworkIdle() {
		for (NetworkInterface networkInterface : this.network) {
			if (networkInterface.getPacketQueue().size() > 0) {
				return false;
			}
		}
		return true;
	}
	
	public void run() {
		for (NetworkInterface networkInterface : this.network) {
			networkInterface.run();
		}
		
		this.nat.run();
	}
}
