package year2019.day6;

import java.util.ArrayList;
import java.util.HashSet;

import general.StringParser;

public class PlanetConnector {
	private HashSet<Planet> planetSet;
	
	public PlanetConnector(HashSet<Planet> planetSet) {
		this.planetSet = planetSet;
	}
	
	public void connect(ArrayList<String> connectionList) {
		
		for (String connectionString : connectionList) {
			ArrayList<String> splitted = StringParser.splitOnSeparator(connectionString, "\\)");
			
			Planet planetA = this.getPlanet(splitted.get(0));
			Planet planetB = this.getPlanet(splitted.get(1));
			
			planetB.setParent(planetA);
			planetA.addChild(planetB);
		}
		
	}
	
	public Planet getPlanet(String name) {
		for (Planet planet : this.planetSet) {
			if (planet.getName().equals(name)) {
				return planet;
			}
		}
		
		return null;
	}
	
}
