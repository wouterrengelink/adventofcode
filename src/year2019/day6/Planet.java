package year2019.day6;

import java.util.ArrayList;
import java.util.HashSet;

public class Planet {
	private String name;
	private Planet parent;
	private ArrayList<Planet> childs;
	
	public Planet(String name) {
		this.name = name;
		this.childs = new ArrayList<Planet>();
	}
	
	public void orbitsAround(Planet parent) {
		this.parent = parent;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Planet getParent() {
		return this.parent;
	}
	
	public void setParent(Planet parent) {
		this.parent = parent;
	}
	
	public void addChild(Planet child) {
		this.childs.add(child);
	}
	
	public boolean isSanta() {
		if (this.name.equals("SAN")) {
			return true;
		}
		
		return false;
	}
	
	public HashSet<Planet> getPathToRoot() {
		HashSet<Planet> path = new HashSet<Planet>();
		Planet tempPlanet = this.getParent();
		
		while(tempPlanet != null) {
			path.add(tempPlanet);
			tempPlanet = tempPlanet.getParent();
		}
		
		return path;
	}

	public int getOrbits() {
		int orbits = 0;
		Planet tempPlanet = this.getParent();
		
		while(tempPlanet != null) {
			orbits++;
			tempPlanet = tempPlanet.getParent();
		}
		
		return orbits;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) return false;
	    if (!(obj instanceof Planet))
	        return false;
	    if (obj == this)
	        return true;
	    
	    return this.name == ((Planet) obj).getName();
	}
	
	@Override
	public int hashCode() {
	    return this.name.hashCode();
	}
	
}
