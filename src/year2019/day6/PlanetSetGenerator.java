package year2019.day6;

import java.util.ArrayList;
import java.util.HashSet;

import general.StringParser;

public class PlanetSetGenerator {
	
	public static HashSet<Planet> generateSet(ArrayList<String> inputList) {
		HashSet<Planet> planetSet = new HashSet<Planet>();
		
		for (String orbitString : inputList) {
			ArrayList<String> names = StringParser.splitOnSeparator(orbitString, "\\)");
			
			Planet firstPlanet = new Planet(names.get(0));
			Planet secondPlanet = new Planet(names.get(1));
			
			planetSet.add(firstPlanet);
			planetSet.add(secondPlanet);
		}
		
		return planetSet;
	}
}
