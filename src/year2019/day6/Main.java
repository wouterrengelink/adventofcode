package year2019.day6;

import java.util.ArrayList;
import java.util.HashSet;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day6/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Initializing day 6 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<String> inputList = inputReader.getInputFromList();
		
		OrbitCalculator orbitCalculator = new OrbitCalculator(inputList);
		
		orbitCalculator.generateSet();
		orbitCalculator.connect();
		
		int orbits = orbitCalculator.calculate();
		
		System.out.println("Total number of direct and indirect orbits is " + orbits);
		
		int numberOfOrbitalTransfersToSanta = orbitCalculator.findNumberOfOrbitalTransfersToFindSanta();
		
		System.out.println("Number of Orbital Transfers to Santa is " + numberOfOrbitalTransfersToSanta);
		
		System.out.println("Done");
	}

}
