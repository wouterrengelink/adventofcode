package year2019.day6;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

class OrbitsTest {

	@Test
	void orbitTest() {
		ArrayList<String> inputList = new ArrayList<String>();
		
		inputList.add("COM)B");
		inputList.add("B)C");
		inputList.add("C)D");
		inputList.add("D)E");
		inputList.add("E)F");
		inputList.add("B)G");
		inputList.add("G)H");
		inputList.add("D)I");
		inputList.add("E)J");
		inputList.add("J)K");
		inputList.add("K)L");
		
		OrbitCalculator orbitCalculator = new OrbitCalculator(inputList);
		
		orbitCalculator.generateSet();
		orbitCalculator.connect();
		
		int orbits = orbitCalculator.calculate();
		
		if (orbits != 42) {
			System.out.println("Orbits: " + orbits);
			fail("Orbit test failed");
		}
	}
	
	@Test
	void findNumberOfOrbitalTransfersToSantaTest() {
		ArrayList<String> inputList = new ArrayList<String>();
		
		inputList.add("COM)B");
		inputList.add("B)C");
		inputList.add("C)D");
		inputList.add("D)E");
		inputList.add("E)F");
		inputList.add("B)G");
		inputList.add("G)H");
		inputList.add("D)I");
		inputList.add("E)J");
		inputList.add("J)K");
		inputList.add("K)L");
		inputList.add("K)YOU");
		inputList.add("I)SAN");
		
		OrbitCalculator orbitCalculator = new OrbitCalculator(inputList);
		
		orbitCalculator.generateSet();
		orbitCalculator.connect();
		
		int numberOfOrbitalTransfersToSanta = orbitCalculator.findNumberOfOrbitalTransfersToFindSanta();
		
		if (numberOfOrbitalTransfersToSanta != 4) {
			System.out.println("Number of Orbital Transfers to Santa: " + numberOfOrbitalTransfersToSanta);
			fail("Santa test failed");
		}
	}

}
