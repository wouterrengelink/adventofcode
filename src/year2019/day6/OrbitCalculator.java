package year2019.day6;

import java.util.ArrayList;
import java.util.HashSet;

public class OrbitCalculator {
	private ArrayList<String> inputList;
	private HashSet<Planet> planetSet;
	private PlanetConnector planetConnector;
	
	public OrbitCalculator(ArrayList<String> inputList) {
		this.inputList = inputList;
	}
	
	public void generateSet() {
		this.planetSet = PlanetSetGenerator.generateSet(this.inputList);
	}
	
	public void connect() {
		this.planetConnector = new PlanetConnector(this.planetSet);
		
		planetConnector.connect(inputList);
	}
	
	public int calculate() {
		int orbits = 0;

		for (Planet planet : this.planetSet) {
			orbits += planet.getOrbits();
		}
		
		return orbits;
	}
	
	public int findNumberOfOrbitalTransfersToFindSanta() {
		// get you planet
		Planet you = this.planetConnector.getPlanet("YOU");
		Planet santa = this.planetConnector.getPlanet("SAN");
		
		HashSet<Planet> youPath = you.getPathToRoot();
		HashSet<Planet> sanPath = santa.getPathToRoot();
		
		HashSet<Planet> parsedYouPath = new HashSet<Planet>(youPath);
		parsedYouPath.removeAll(sanPath);
		
		HashSet<Planet> parsedSanPath = new HashSet<Planet>(sanPath);
		parsedSanPath.removeAll(youPath);
		
		int youTransfers = parsedYouPath.size();
		int santaTransfers = parsedSanPath.size();
		
		return youTransfers + santaTransfers;
	}
}
