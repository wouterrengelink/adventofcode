package year2019.day16;

import java.util.ArrayList;

public class FlawedFrequencyTransmission {
	private int numberOfPhases;
	
	public FlawedFrequencyTransmission(int numberOfPhases) {
		this.numberOfPhases = numberOfPhases;
	}
	
	public ArrayList<Integer> getOutput(ArrayList<Integer> inputList) {
		ArrayList<Integer> temporaryList = inputList;
		for (int i = 0; i < this.numberOfPhases; i++) {
			Phase phase = new Phase(temporaryList);
			temporaryList = phase.getOutput();
		}
		
		return temporaryList;
	}
}
