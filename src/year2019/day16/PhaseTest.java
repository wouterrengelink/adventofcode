package year2019.day16;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import general.StringParser;

class PhaseTest {

	@Test
	void test12345678() {
		String input = "12345678";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		Phase phase = new Phase(inputList);
		
		ArrayList<Integer> outputList = phase.getOutput();
		
		ArrayList<Integer> solutionList = new ArrayList<Integer>();
		solutionList.add(4);
		solutionList.add(8);
		solutionList.add(2);
		solutionList.add(2);
		solutionList.add(6);
		solutionList.add(1);
		solutionList.add(5);
		solutionList.add(8);
		
		if (!outputList.equals(solutionList)) {
			fail("Output not the solution");
		}
	}
	
	@Test
	void test48226158() {
		String input = "48226158";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		Phase phase = new Phase(inputList);
		
		ArrayList<Integer> outputList = phase.getOutput();
		
		ArrayList<Integer> solutionList = new ArrayList<Integer>();
		solutionList.add(3);
		solutionList.add(4);
		solutionList.add(0);
		solutionList.add(4);
		solutionList.add(0);
		solutionList.add(4);
		solutionList.add(3);
		solutionList.add(8);
		
		if (!outputList.equals(solutionList)) {
			fail("Output not the solution");
		}
	}
	
	@Test
	void test34040438() {
		String input = "34040438";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		Phase phase = new Phase(inputList);
		
		ArrayList<Integer> outputList = phase.getOutput();
		
		ArrayList<Integer> solutionList = new ArrayList<Integer>();
		solutionList.add(0);
		solutionList.add(3);
		solutionList.add(4);
		solutionList.add(1);
		solutionList.add(5);
		solutionList.add(5);
		solutionList.add(1);
		solutionList.add(8);
		
		if (!outputList.equals(solutionList)) {
			fail("Output not the solution");
		}
	}
	
	@Test
	void test03415518() {
		String input = "03415518";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		Phase phase = new Phase(inputList);
		
		ArrayList<Integer> outputList = phase.getOutput();
		
		ArrayList<Integer> solutionList = new ArrayList<Integer>();
		solutionList.add(0);
		solutionList.add(1);
		solutionList.add(0);
		solutionList.add(2);
		solutionList.add(9);
		solutionList.add(4);
		solutionList.add(9);
		solutionList.add(8);
		
		if (!outputList.equals(solutionList)) {
			fail("Output not the solution");
		}
	}

}
