package year2019.day16;

import java.util.ArrayList;

public class Phase {
	private ArrayList<Integer> basePattern;
	private ArrayList<Integer> computePattern;
	private ArrayList<Integer> inputList;
	private int patternIterator;
	private int basePatternIterator;
	
	public Phase(ArrayList<Integer> inputList) {
		this.inputList = inputList;
		this.basePattern = new ArrayList<Integer>();
		this.basePattern.add(0);
		this.basePattern.add(1);
		this.basePattern.add(0);
		this.basePattern.add(-1);
		
		this.computePattern = new ArrayList<Integer>(this.basePattern);
		
		this.patternIterator = 1;
		this.basePatternIterator = 1;
	}
	
	private int getBasePatternValue() {
		if (this.patternIterator >= this.computePattern.size()) {
			this.patternIterator = 0;
		}
		
		return this.computePattern.get(this.patternIterator++);
	}
	
	private void adjustBasePattern() {
		this.computePattern = new ArrayList<Integer>();
		
		for (int integer : this.basePattern) {
			for (int i = 0; i <= this.basePatternIterator; i++) {
				this.computePattern.add(integer);
			}
		}
		
		this.basePatternIterator++;
	}
	
	public ArrayList<Integer> getOutput() {
		ArrayList<Integer> output = new ArrayList<Integer>();
		
		for (int i = 0; i < this.inputList.size(); i++) {

			int finalValue = 0;
			for(int integer : this.inputList) {
				int basePatternValue = this.getBasePatternValue();
				finalValue += integer * basePatternValue;
			}
			
			int outCome = this.getTenValue(finalValue);
			output.add(outCome);
			
			this.adjustBasePattern();
			this.patternIterator = 1;
		}
		
		return output;
	}
	
	private int getTenValue(int value) {
		String valueString = "" + value;
		
		String lastCharacter = "" + valueString.charAt(valueString.length() - 1);
		
		return Integer.parseInt(lastCharacter);
	}
}
