package year2019.day16;

import java.util.ArrayList;
import java.util.List;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day16/input.txt";

	public static void main(String[] args) {
		System.out.println("Day 16 2019");

		InputReader inputReader = new InputReader(inputFile);
		
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(inputReader.getInputFromCharacters());
		ArrayList<Integer> copyList = new ArrayList<Integer>(inputList);
		// there could be errors in this code, shouldnt we start at i = 1?
		for (int i = 0; i < 1; i++) {
			inputList.addAll(copyList);
		}
		
		List<Integer> messageOffset = inputList.subList(0, 7);
		System.out.println("MessageOffset " + messageOffset);
		
		FlawedFrequencyTransmission flawedFrequencyTransmission = new FlawedFrequencyTransmission(100);
		
		ArrayList<Integer> output = flawedFrequencyTransmission.getOutput(inputList);
		
		List<Integer> firstEightDigitsOutput = output.subList(0, 8);
		
		System.out.println(firstEightDigitsOutput);
		
		System.out.println("Done");
	}

}
