package year2019.day16;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import general.StringParser;

class FlawedFrequencyTransmissionTest {

	@Test
	void test() {
		String input = "12345678";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		FlawedFrequencyTransmission flawedFrequencyTransmission = new FlawedFrequencyTransmission(4);
		
		ArrayList<Integer> output = flawedFrequencyTransmission.getOutput(inputList);
		
		ArrayList<Integer> solution = new ArrayList<Integer>();
		solution.add(0);
		solution.add(1);
		solution.add(0);
		solution.add(2);
		solution.add(9);
		solution.add(4);
		solution.add(9);
		solution.add(8);
		
		if (!output.equals(solution)) {
			fail("Output is not equal to the solution");
		}
	}
	
	@Test
	void largeTest() {
		String input = "80871224585914546619083218645595";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		FlawedFrequencyTransmission flawedFrequencyTransmission = new FlawedFrequencyTransmission(100);
		
		ArrayList<Integer> output = flawedFrequencyTransmission.getOutput(inputList);
		
		List<Integer> firstEightDigitsOutput = output.subList(0, 8);
		
		ArrayList<Integer> solution = new ArrayList<Integer>();
		solution.add(2);
		solution.add(4);
		solution.add(1);
		solution.add(7);
		solution.add(6);
		solution.add(1);
		solution.add(7);
		solution.add(6);
		
		if (!firstEightDigitsOutput.equals(solution)) {
			fail("Output is not the solution");
		}
	}
	
	@Test
	void largeTest2() {
		String input = "19617804207202209144916044189917";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		FlawedFrequencyTransmission flawedFrequencyTransmission = new FlawedFrequencyTransmission(100);
		
		ArrayList<Integer> output = flawedFrequencyTransmission.getOutput(inputList);
		
		List<Integer> firstEightDigitsOutput = output.subList(0, 8);
		
		ArrayList<Integer> solution = new ArrayList<Integer>();
		solution.add(7);
		solution.add(3);
		solution.add(7);
		solution.add(4);
		solution.add(5);
		solution.add(4);
		solution.add(1);
		solution.add(8);
		
		if (!firstEightDigitsOutput.equals(solution)) {
			fail("Output is not the solution");
		}
	}
	
	@Test
	void largeTest3() {
		String input = "69317163492948606335995924319873";
		ArrayList<Integer> inputList = StringParser.convertToIntegerList(StringParser.parseStringToCharacterList(input));
		
		FlawedFrequencyTransmission flawedFrequencyTransmission = new FlawedFrequencyTransmission(100);
		
		ArrayList<Integer> output = flawedFrequencyTransmission.getOutput(inputList);
		
		List<Integer> firstEightDigitsOutput = output.subList(0, 8);
		
		ArrayList<Integer> solution = new ArrayList<Integer>();
		solution.add(5);
		solution.add(2);
		solution.add(4);
		solution.add(3);
		solution.add(2);
		solution.add(1);
		solution.add(3);
		solution.add(3);
		
		if (!firstEightDigitsOutput.equals(solution)) {
			fail("Output is not the solution");
		}
	}

}
