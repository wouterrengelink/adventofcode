package year2019.day5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Computer {
	private ArrayList<Integer> program;
	private ArrayList<Integer> initialProgram;
	private int instructionPointer;
	
	public Computer(ArrayList<Integer> program) {
		this.initialProgram = program;
		this.reset();
	}
	
	public void reset() {
		this.program = new ArrayList<Integer>(this.initialProgram);
		this.instructionPointer = 0;
	}
	
	public void run() {
		boolean programHalted = false;
		
		while (!programHalted) {
			Integer value = this.program.get(this.instructionPointer);
			
			Instruction instruction = InstructionParser.parse(value);
			
			switch(instruction.getOpCode()) {
				case 1:
					this.add(instruction);
					this.instructionPointer += 4;
					break;
				case 2:
					this.multiply(instruction);
					this.instructionPointer += 4;
					break;
				case 3:
					this.requestInput();
					this.instructionPointer += 2;
					break;
				case 4:
					this.showOutput(instruction);
					this.instructionPointer += 2;
					break;
				case 5:
					if (!this.jumpIfTrue(instruction)) {
						this.instructionPointer += 3;
					}
					break;
				case 6:
					if (!this.jumpIfFalse(instruction)) {
						this.instructionPointer += 3;
					}
					break;
				case 7:
					this.lessThan(instruction);
					this.instructionPointer += 4;
					break;
				case 8:
					this.isEqual(instruction);
					this.instructionPointer += 4;
					break;
				default:
					programHalted = true;
					this.instructionPointer += 1;
			}
		}
	}
	
	public void setValues(int noun, int verb) {
		this.program.set(1, noun);
		this.program.set(2, verb);
	}
	
	private boolean jumpIfTrue(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		
		if (firstValue > 0) {
			this.instructionPointer = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
			
			return true;
		}
		
		return false;
	}
	
	private boolean jumpIfFalse(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		
		if (firstValue == 0) {
			this.instructionPointer = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
			
			return true;
		}
		
		return false;
	}
	
	private void lessThan(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		int resultPosition = this.program.get(this.instructionPointer + 3);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		int secondValue = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		if (firstValue < secondValue) {
			this.program.set(resultPosition, 1);
		} else {
			this.program.set(resultPosition, 0);
		}
	}
	
	private void isEqual(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		int resultPosition = this.program.get(this.instructionPointer + 3);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		int secondValue = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		if (firstValue == secondValue) {
			this.program.set(resultPosition, 1);
		} else {
			this.program.set(resultPosition, 0);
		}
	}
	
	private void multiply(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		int resultPosition = this.program.get(this.instructionPointer + 3);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		int secondValue = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		int multipliedValue = firstValue * secondValue;
		
		this.program.set(resultPosition, multipliedValue);
	}
	
	private void add(Instruction instruction) {
		int firstPosition = this.program.get(this.instructionPointer + 1);
		int secondPosition = this.program.get(this.instructionPointer + 2);
		int resultPosition = this.program.get(this.instructionPointer + 3);
		
		int firstValue = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), firstPosition);
		int secondValue = ParameterModeParser.getValue(this.program, instruction.getSecondParameterMode(), secondPosition);
		
		int addedValue = firstValue + secondValue;
		
		this.program.set(resultPosition, addedValue);
	}
	
	private void requestInput() {
		System.out.println("System requests input:");
		
		int resultPosition = this.program.get(this.instructionPointer + 1);
		
//		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//		String input = "";
//		try {
//			input = bufferedReader.readLine();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		this.program.set(resultPosition, Integer.parseInt(input));
		this.program.set(resultPosition, 5);
	}
	
	private String showOutput(Instruction instruction) {
		int outputPosition = this.program.get(this.instructionPointer + 1);
		
		int value = ParameterModeParser.getValue(this.program, instruction.getFirstParameterMode(), outputPosition);
		
		System.out.println("Output: " + value);
		
		return "" + value;
	}
	
	public int getValueAtIndex(int index) {
		return this.program.get(index);
	}
	
	public int getValueAtZero() {
		return this.program.get(0);
	}
}
