package year2019.day5;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class ComputerTest {

	@Test
	void positionModeTest() {
		ArrayList<Integer> program = new ArrayList<Integer>();
		program.add(3);
		program.add(12);
		program.add(6);
		program.add(12);
		program.add(15);
		program.add(1);
		program.add(13);
		program.add(14);
		program.add(13);
		program.add(4);
		program.add(13);
		program.add(99);
		program.add(-1);
		program.add(0);
		program.add(1);
		program.add(9);
		
		Computer computer = new Computer(program);
		
		System.out.println("Position output");
		computer.run();
	}
	
	@Test
	void immediateModeTest() {
		ArrayList<Integer> program = new ArrayList<Integer>();
		program.add(3);
		program.add(3);
		program.add(1105);
		program.add(-1);
		program.add(9);
		program.add(1101);
		program.add(0);
		program.add(0);
		program.add(12);
		program.add(4);
		program.add(12);
		program.add(99);
		program.add(1);
		
		Computer computer = new Computer(program);
		
		System.out.println("Immediate output");
		computer.run();
	}

}
