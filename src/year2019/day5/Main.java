package year2019.day5;

import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {

	private static String inputFile = "./src/year2019/day5/input.txt";
	
	public static void main(String[] args) {
		System.out.println("Booting up computer");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<Integer> program = StringParser.convertToIntegerList(inputReader.getInputFromSeparator(","));
		
		Computer computer = new Computer(program);
		
		computer.run();
		
		System.out.println("Done");
	}

}
