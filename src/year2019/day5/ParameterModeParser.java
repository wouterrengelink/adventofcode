package year2019.day5;

import java.util.ArrayList;

public class ParameterModeParser {

	public static ParameterMode parseParameterMode(int parameterMode) {
		switch (parameterMode) {
			case 0:
				return ParameterMode.POSITION;
			case 1:
				return ParameterMode.IMMEDIATE;
		}
		
		return null;
	}
	
	public static int getValue(ArrayList<Integer> program, ParameterMode parameterMode, int programValue) {
		int value = -1;
		
		if (parameterMode == ParameterMode.POSITION) {
			value = program.get(programValue);
		} else if (parameterMode == ParameterMode.IMMEDIATE) {
			value = programValue;
		}
		
		return value;
	}
}
