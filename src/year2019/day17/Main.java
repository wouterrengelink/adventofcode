package year2019.day17;

import java.math.BigInteger;
import java.util.ArrayList;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day17/input.txt";
	private static int memorySize = 4096;
	
	public static void main(String[] args) {
		System.out.println("Day 17 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		
		Robot robot = new Robot(program, memorySize);
		MapViewer mapViewer = new MapViewer(robot.getMap(), robot.getPosition());

		robot.run();

		ArrayList<Coordinate> intersections = robot.getIntersections();
		
		int alignMentParameters = robot.getAlignmentParameters();
		
		System.out.println("Alignment parameters are " + alignMentParameters);
		
		System.out.println("Done");
	}

}
