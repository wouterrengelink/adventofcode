package year2019.day17;

public enum TileType {
	SCAFFOLD,
	EMPTY,
	UNKNOWN,
	INTERSECTION,
	OTHER,
	DEATH
}
