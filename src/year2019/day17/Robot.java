package year2019.day17;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

public class Robot {
	private Computer computer;
	private HashMap<Coordinate, Tile> map;
	private ArrayList<Coordinate> traveledList;
	private Coordinate position;
	private int previousOutput;
	
	public Robot(ArrayList<BigInteger> program, int memorySize) {
		this.computer = new Computer(program, memorySize);
		this.map = new HashMap<Coordinate, Tile>();
		this.position = new Coordinate(0, 0);
		this.traveledList = new ArrayList<Coordinate>();
		this.computer.setValueAtIndex(new BigInteger("2"), 0);
		this.previousOutput = 0;
		// main routine

//		this.computer.addInput(76); // R
//		this.computer.addInput(44); // ,
//		this.computer.addInput(56); // 8
//		this.computer.addInput(10); // NL
//		
//		this.computer.addInput(76); // R
//		this.computer.addInput(44); // ,
//		this.computer.addInput(56); // 8
//		this.computer.addInput(10); // NL
	}
	
	public void run() {
		int x = 0;
		int y = 0;
		String outputMessage = "";
				
		this.computer.addAsciiInput("A");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("B");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("C");
		this.computer.addAsciiInput("\n");
		
		// go 11 places left
		// implement function a
		this.computer.addAsciiInput("L");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("9");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("3");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("L");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("1");
		this.computer.addAsciiInput("\n");
		
		// go 5 places down
		// implement function b
		this.computer.addAsciiInput("0");
		this.computer.addAsciiInput(",");
		this.computer.addAsciiInput("0");
		this.computer.addAsciiInput("\n");
		
		// implement function c
		this.computer.addAsciiInput("0");
		this.computer.addAsciiInput("\n");
		
		
		
		
		// enable live feed
		this.computer.addAsciiInput("y");
		this.computer.addAsciiInput("\n");
		
		
		BigInteger dustCollected = new BigInteger("0");
		boolean firstTimeCharacter = true;
		
		while (!this.computer.isHalted()) {
			BigInteger bigOutput = this.computer.run();
			
			if (bigOutput == null) {
				this.locateIntersections();
				System.out.println("Dust collected " + dustCollected);
				System.out.println(outputMessage);
				return;
			} else {
				dustCollected = bigOutput;
			}
			 
			int output = bigOutput.intValue();
			
//			
			
			
			boolean hackedVideoFeed = true;
			switch (output) {
				case 63: // ?
					System.out.println("? character hit.");
					
					break;
				case 88: // X
					this.map.put(new Coordinate(x, y), new Tile(TileType.DEATH));
					System.out.println("DEATH!");
					System.exit(0);
					break;
				case 60: //<
					if (firstTimeCharacter) {
						firstTimeCharacter = false;
					} else {
						this.position.setX(this.position.getX() - 1);
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 62: //>

					this.position.setX(this.position.getX() + 1);

					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 118: // v
					System.out.println("Move downwards");
					if (hackedVideoFeed) {
						hackedVideoFeed = false;
					} else {
						System.out.println("Moving downwards");

						this.position.setY(this.position.getY() + 1);

					}
					break;
				case 35: // #
					this.map.put(new Coordinate(x, y), new Tile(TileType.SCAFFOLD));
					break;
				case 46: // .
					this.map.put(new Coordinate(x, y), new Tile(TileType.EMPTY));
					break;
				case 10: // new line
					y++;
					x = 0;
					continue;
				case 94: // ^
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Moving downwards");
					this.position.setY(this.position.getY() - 1);
					
					this.position.setX(x);
					this.position.setY(y);
					
					this.map.put(new Coordinate(x, y), new Tile(TileType.UNKNOWN));
					break;
				default:
					outputMessage += (char) output;
					// System.out.println("Other input found " + (char) output);
				
				previousOutput = output;
			}
			x++;
		}
	}
	
	private void locateIntersections() {
		System.out.println("Locating intersections");
		for (HashMap.Entry<Coordinate, Tile> entry : this.map.entrySet()) {
			Coordinate thisCoordinate = entry.getKey();
			Tile tile = entry.getValue();
			
			if (tile.getType() == TileType.EMPTY) {
				continue;
			}
			
			Coordinate leftCoordinate = new Coordinate(thisCoordinate.getX() - 1, thisCoordinate.getY());
			Tile leftTile = this.map.get(leftCoordinate);
			if (leftTile == null || leftTile.getType() == TileType.EMPTY) {
				continue;
			}

			Coordinate rightCoordinate = new Coordinate(thisCoordinate.getX() + 1, thisCoordinate.getY());
			Tile rightTile = this.map.get(rightCoordinate);
			if (rightTile == null || rightTile.getType() == TileType.EMPTY) {
				continue;
			}
			
			Coordinate topCoordinate = new Coordinate(thisCoordinate.getX(), thisCoordinate.getY() - 1);
			Tile topTile = this.map.get(topCoordinate);
			if (topTile == null || topTile.getType() == TileType.EMPTY) {
				continue;
			}
			
			Coordinate bottomCoordinate = new Coordinate(thisCoordinate.getX(), thisCoordinate.getY() + 1);
			Tile bottomTile = this.map.get(bottomCoordinate);
			if (bottomTile == null || bottomTile.getType() == TileType.EMPTY) {
				continue;
			}
			
			this.map.put(thisCoordinate, new Tile(TileType.INTERSECTION));
//			
//			if (tile.getType() != TileType.SCAFFOLD && tile.getType() != TileType.INTERSECTION) {
//				continue;
//			}
//			
//			
//			// get coordinates around it
//			Coordinate leftCoordinate = new Coordinate(thisCoordinate.getX() - 1, thisCoordinate.getY());
//			Coordinate rightCoordinate = new Coordinate(thisCoordinate.getX() + 1, thisCoordinate.getY());
//			Coordinate topCoordinate = new Coordinate(thisCoordinate.getX(), thisCoordinate.getY() - 1);
//			Coordinate bottomCoordinate = new Coordinate(thisCoordinate.getX(), thisCoordinate.getY() + 1);
//			
//			if (this.isScaffoldOnPosition(rightCoordinate)) {
//				// this coordinate is an intersection
//				this.map.put(thisCoordinate, new Tile(TileType.INTERSECTION));
//			}
//			
			
		}
	}
	
	public ArrayList<Coordinate> getIntersections() {
		ArrayList<Coordinate> intersections = new ArrayList<Coordinate>();
		
		for (HashMap.Entry<Coordinate, Tile> entry : this.map.entrySet()) {
			Tile tile = entry.getValue();
			
			if (tile.getType() != TileType.INTERSECTION) {
				continue;
			}
			
			intersections.add(entry.getKey());
		}
		
		return intersections;
	}
	
	public int getAlignmentParameters() {
		int alignMentParameters = 0;
		
		for (HashMap.Entry<Coordinate, Tile> entry : this.map.entrySet()) {
			Tile tile = entry.getValue();
			
			if (tile.getType() != TileType.INTERSECTION) {
				continue;
			}
			
			Coordinate intersectionCoordinate = entry.getKey();
			
			alignMentParameters += this.calculateAlignmentForPosition(intersectionCoordinate);
		}
		
		return alignMentParameters;
	}
	
	public int calculateAlignmentForPosition(Coordinate coordinate) {
		return coordinate.getX() * coordinate.getY();
	}
	
	private boolean isScaffoldOnPosition(Coordinate coordinate) {
		if (this.map.get(coordinate) != null && this.map.get(coordinate).getType() == TileType.SCAFFOLD) {
			return true;
		}
		
		return false;
	}
	
	public HashMap<Coordinate, Tile> getMap() {
		return this.map;
	}
	
	public Coordinate getPosition() {
		return this.position;
	}
	
	public void reset() {
		this.computer.reset();
	}
}
