package year2019.day17;

public class Tile {
	private TileType tileType;
	
	public Tile(TileType tileType) {
		this.tileType = tileType;
	}
	
	public TileType getType() {
		return this.tileType;
	}
	
}
