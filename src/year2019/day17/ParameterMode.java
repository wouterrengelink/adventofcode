package year2019.day17;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
