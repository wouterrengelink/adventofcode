package year2019.day19;

public enum TileType {
	EMPTY,
	BEAM,
	SIMULATED_BEAM,
	SIMULATED_BEAM_MISS
}
