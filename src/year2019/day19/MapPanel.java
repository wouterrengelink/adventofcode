package year2019.day19;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MapPanel extends JPanel implements ActionListener {
	private HashMap<Coordinate, Tile> map;
	private Timer updateTimer;
	
	private static int xOffSet = 50;
	private static int yOffSet = 50;
	
	public MapPanel(HashMap<Coordinate, Tile> map) {
		this.map = map;
		
        this.updateTimer = new Timer(10, this);
        this.updateTimer.start();
	}
	
	@Override
    public void paint(Graphics g) 
    {
		for(HashMap.Entry<Coordinate, Tile> entry : this.map.entrySet()) {
			Coordinate coordinate = entry.getKey();
			Tile tile = entry.getValue();
			
			switch (tile.getType()) {
				case BEAM:
					g.setColor(Color.YELLOW);
					break;
				case EMPTY:
					g.setColor(Color.BLACK);
					break;
				case SIMULATED_BEAM:
					g.setColor(Color.GREEN);
					break;
				case SIMULATED_BEAM_MISS:
					g.setColor(Color.RED);
					break;
			}
			
			g.fillRect(coordinate.getX() * 20 + MapPanel.xOffSet, coordinate.getY() * 20 + MapPanel.yOffSet, 20, 20);
			
			if (tile.getType() == TileType.BEAM) {
				g.setColor(Color.BLUE);
				g.setFont(new Font("TimesRoman", Font.PLAIN, 10));
				g.drawString("x:" + coordinate.getX(), coordinate.getX() * 20 + MapPanel.xOffSet, coordinate.getY() * 20 + MapPanel.yOffSet + 5);
				g.drawString("y:" + coordinate.getY(), coordinate.getX() * 20 + MapPanel.xOffSet, coordinate.getY() * 20 + MapPanel.yOffSet + 15);
			}
		}
    }
	
	private void saveImage() {
		BufferedImage bi = new BufferedImage(this.getSize().width, this.getSize().height, BufferedImage.TYPE_INT_ARGB); 
		Graphics g = bi.createGraphics();
		this.paint(g);  //this == JComponent
		g.dispose();
		try{ImageIO.write(bi,"png",new File("test.png"));}catch (Exception e) {}
	}
	
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == this.updateTimer) {
			this.repaint();
		}
	}
}
