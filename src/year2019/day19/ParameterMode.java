package year2019.day19;

public enum ParameterMode {
	POSITION,
	IMMEDIATE,
	RELATIVE
}
