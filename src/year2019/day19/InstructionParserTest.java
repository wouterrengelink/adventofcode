package year2019.day19;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InstructionParserTest {

	@Test
	void testFourCharacters() {
		Instruction instruction = InstructionParser.parse(1101);
		
		if (instruction.getOpCode() != 1) {
			fail("OpCode not 1");
		}
		
		if (instruction.getFirstParameterMode() != ParameterMode.IMMEDIATE) {
			fail("First parameter mode not immediate");
		}
		
		if (instruction.getSecondParameterMode() != ParameterMode.IMMEDIATE) {
			fail("Second parameter mode not immediate");
		}
		
		if (instruction.getThirdParameterMode() != ParameterMode.POSITION) {
			fail("Third parameter mode not position");
		}
	}
	
	@Test
	void testThreeCharacters() {
		Instruction instruction = InstructionParser.parse(101);
		
		if (instruction.getOpCode() != 1) {
			fail("OpCode not 1");
		}
		
		if (instruction.getFirstParameterMode() != ParameterMode.IMMEDIATE) {
			fail("First parameter mode not immediate");
		}
		
		if (instruction.getSecondParameterMode() != ParameterMode.POSITION) {
			fail("Second parameter mode not position");
		}
		
		if (instruction.getThirdParameterMode() != ParameterMode.POSITION) {
			fail("Third parameter mode not position");
		}
	}
	
	@Test
	void testTwoCharacters() {
		Instruction instruction = InstructionParser.parse(99);
		
		if (instruction.getOpCode() != 99) {
			fail("OpCode not 99");
		}
		
		if (instruction.getFirstParameterMode() != ParameterMode.POSITION) {
			fail("First parameter mode not position");
		}
		
		if (instruction.getSecondParameterMode() != ParameterMode.POSITION) {
			fail("Second parameter mode not position");
		}
		
		if (instruction.getThirdParameterMode() != ParameterMode.POSITION) {
			fail("Third parameter mode not position");
		}
	}
	
	@Test
	void testOneCharacters() {
		Instruction instruction = InstructionParser.parse(4);
		
		if (instruction.getOpCode() != 4) {
			fail("OpCode not 4");
		}
		
		if (instruction.getFirstParameterMode() != ParameterMode.POSITION) {
			fail("First parameter mode not position");
		}
		
		if (instruction.getSecondParameterMode() != ParameterMode.POSITION) {
			fail("Second parameter mode not position");
		}
		
		if (instruction.getThirdParameterMode() != ParameterMode.POSITION) {
			fail("Third parameter mode not position");
		}
	}

}
