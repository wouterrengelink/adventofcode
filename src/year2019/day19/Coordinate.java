package year2019.day19;

public class Coordinate {
	private int x;
	private int y;
	
	public Coordinate(Coordinate coordinate) {
		this.x = coordinate.getX();
		this.y = coordinate.getY();
	}
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public boolean equals(Object object) { 
		if (object == this) { 
		    return true; 
		} 
	  
	    if (!(object instanceof Coordinate)) { 
	        return false; 
	    } 
	      
	    Coordinate coordinate = (Coordinate) object; 
	      
	    if (this.getX() == coordinate.getX() && this.getY() == coordinate.getY()) {
	    	return true;
	    }
	    
	    return false;
	} 
	
	@Override
	public int hashCode() {
		return (this.x + "_" + this.y).hashCode();
	}
	
	public String toString() {
		return "x: " + this.x + " y: " + this.y;
	}
}
