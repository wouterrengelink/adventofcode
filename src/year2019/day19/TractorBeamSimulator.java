package year2019.day19;

import java.util.HashMap;

public class TractorBeamSimulator {
	private HashMap<Coordinate, Tile> map;
	private int depth;
	private SpaceShipCalculator spaceShipCalculator;
	
	public TractorBeamSimulator(SpaceShipCalculator spaceShipCalculator, HashMap<Coordinate, Tile> map, int depth) {
		this.map = map;
		this.depth = depth;
		this.spaceShipCalculator = spaceShipCalculator;
	}
	
	public void simulate() {
		this.simulateLowerBeam();
		this.simulateUpperBeam();
	}
	
	private void simulateLowerBeam() {
		int iterator = 0;
		int y = 0;
		
		for (int x = 0; x < this.depth; x++) {			
			if (x % 4 == 0) {
				iterator++;
			}
			
			int finalY = y + iterator - 1; 
			
			if (this.spaceShipCalculator.isBeamPoint(x, finalY)) {
				System.out.println("HIT");
			} else {
				System.out.println("MISS");
			}

			this.putTile(x, finalY);
			y++;
		}
	}
	
	private void simulateUpperBeam() {
		int iterator = 0;
		int y = 0;
		
		for (int x = 0; x < this.depth; x++) {			
			if (x % 8 == 0) {
				iterator++;
			}
			
			int finalY = y + iterator;
			
			if (this.spaceShipCalculator.isBeamPoint(x, finalY)) {
				System.out.println("HIT");
			} else {
				System.out.println("MISS");
			}
			
			this.putTile(x, y + finalY);
			y++;
		}
	}
	
	private void putTile(int x, int y) {
		Coordinate coordinate = new Coordinate(x, y);
		Tile currentTile = this.map.get(coordinate);
		
		if (currentTile == null) {
			return;
		}
		
		if (currentTile.getType() == TileType.BEAM) {
			this.map.put(coordinate, new Tile(TileType.SIMULATED_BEAM));
		} else {
			this.map.put(coordinate, new Tile(TileType.SIMULATED_BEAM_MISS));
		}
	}
	
	// upper beam
	// x: 43 y: 49
	// gradient = y / x
	
	// lower beam
	// x: 40 y: 49
	// gradient = y / x

}
