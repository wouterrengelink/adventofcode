package year2019.day19;

import java.math.BigInteger;
import java.util.HashMap;

public class SpaceShipCalculator {
	private Computer computer;
	private int height;
	private int width;
	HashMap<Coordinate, Tile> map;
	
	public SpaceShipCalculator(Computer computer, int width, int height, HashMap<Coordinate, Tile> map) {
		this.computer = computer;
		this.width = width;
		this.height = height;
		this.map = map;
	}

	public boolean isBeamPoint(int x, int y) {
		this.computer.addInput(new BigInteger("" + x));
		this.computer.addInput(new BigInteger("" + y));
		
		int output = this.computer.run().intValue();
		this.computer.reset();
		
		if (output == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	private void addToMap(int x, int y, TileType tileType) {
		this.map.put(new Coordinate(x, y), new Tile(tileType));
	}
	
	public void scanBeam(int x, int y) {
		this.computer.addInput(new BigInteger("" + x));
		this.computer.addInput(new BigInteger("" + y));
		
		int output = this.computer.run().intValue();
		this.computer.reset();
		
		if (output == 1) {
			this.addToMap(x, y, TileType.BEAM);
		} else {
			this.addToMap(x, y, TileType.EMPTY);
		}
	}
	
	public boolean isInTractorBeam(int x, int y) {
		this.computer.addInput(new BigInteger("" + x));
		this.computer.addInput(new BigInteger("" + y));
		
		int output = this.computer.run().intValue();
		this.computer.reset();
		
		
		
		if (output != 1) {
			this.addToMap(x, y, TileType.EMPTY);
			return false;
		} else {
			this.addToMap(x, y, TileType.BEAM);
		}
		
		int searchX = x + this.width;
		int searchY = y;
		this.computer.addInput(new BigInteger("" + searchX));
		this.computer.addInput(new BigInteger("" + searchY));
		output = this.computer.run().intValue();
		this.computer.reset();
		
		if (output != 1) {
			this.addToMap(searchX, searchY, TileType.EMPTY);
			return false;
		} else {
			this.addToMap(searchX, searchY, TileType.BEAM);
		}
		
		searchX = x;
		searchY = y + this.height;
		this.computer.addInput(new BigInteger("" + searchX));
		this.computer.addInput(new BigInteger("" + searchY));
		output = this.computer.run().intValue();
		this.computer.reset();
		
		if (output != 1) {
			this.addToMap(searchX, searchY, TileType.EMPTY);
			return false;
		} else {
			this.addToMap(searchX, searchY, TileType.BEAM);
		}
		
		searchX = x + this.width;
		searchY = y + this.height;
		this.computer.addInput(new BigInteger("" + searchX));
		this.computer.addInput(new BigInteger("" + searchY));
		output = this.computer.run().intValue();
		this.computer.reset();
		
		if (output != 1) {
			this.addToMap(searchX, searchY, TileType.EMPTY);
			return false;
		} else {
			this.addToMap(searchX, searchY, TileType.BEAM);
		}
		
		return true;
	}
	
	
}
