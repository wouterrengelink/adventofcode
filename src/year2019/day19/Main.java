package year2019.day19;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import general.InputReader;
import general.StringParser;

public class Main {
	private static String inputFile = "./src/year2019/day19/input.txt";
	private static int depth = 700;
	
	public static void main(String[] args) {
		System.out.println("Day 19 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<BigInteger> program = StringParser.convertToBigIntegerList(inputReader.getInputFromSeparator(","));
		
		Computer computer = new Computer(program, 4000000);
		
		HashMap<Coordinate, Tile> map = new HashMap<Coordinate, Tile>();
		
		SpaceShipCalculator spaceShipCalculator = new SpaceShipCalculator(computer, 100, 100, map);
		
		MapViewer mapViewer = new MapViewer(map);
		
//		for (int x = 0; x < Main.depth; x++)
//		for (int y = 0; y < Main.depth; y++) {
//			spaceShipCalculator.scanBeam(x, y);
//		}
		
		TractorBeamSimulator tractorBeamSimulator = new TractorBeamSimulator(spaceShipCalculator, map, Main.depth);
		
		tractorBeamSimulator.simulate();
	
		System.out.println("Done");
	}

}
