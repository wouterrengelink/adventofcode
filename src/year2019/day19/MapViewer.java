package year2019.day19;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.Timer;

public class MapViewer extends JFrame  {
	public MapViewer(HashMap<Coordinate, Tile> map) {
		this.setSize(1200, 1000);
        this.setTitle("Advent of Code - Viewer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        MapPanel mapPanel = new MapPanel(map);
//        JScrollPane jScrollPane = new JScrollPane(mapPanel);
//        
//        jScrollPane.setPreferredSize(new Dimension(-1, 100));
//        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//        
        this.add(mapPanel);
	}
	
	
}
