package year2019.day10;

import java.util.ArrayList;
import java.util.HashMap;

public class MapScanner {
	private ArrayList<String> map;
	
	public MapScanner(ArrayList<String> map) {
		this.map = map;
	}
	
	public Map getAsteroidMap() {
		HashMap<Coordinate, Asteroid> asteroidMap = new HashMap<Coordinate, Asteroid>();
		int yCoordinate = 0;
		
		for (String mapSlice : this.map) {
			this.parseMapSlice(asteroidMap, mapSlice, yCoordinate++);
		}
		
		return new Map(asteroidMap);
	}
	
	private void parseMapSlice(HashMap<Coordinate, Asteroid> asteroidMap, String mapSlice, int yCoordinate) {
		for (int x = 0; x < mapSlice.length(); x++) {
			switch(mapSlice.charAt(x)) {
				case '.':
					continue;
				case '#':
					Asteroid asteroid = new Asteroid(x, yCoordinate);
					asteroidMap.put(new Coordinate(x, yCoordinate), asteroid);
					break;
			}
		}
	}
	
	public int getHeight() {
		return this.map.size();
	}
	
	public int getWidth() {
		String mapSlice = this.map.get(0);
		return mapSlice.length();
	}
	
}
