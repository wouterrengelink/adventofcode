package year2019.day10;

import java.util.ArrayList;

public class Asteroid {
	private Coordinate position;
	private MapStyle mapStyle;
	
	public Asteroid(int x, int y) {
		this.position = new Coordinate(x, y);
		this.mapStyle = MapStyle.IDLE;
	}
	
	public void setMapStyle(MapStyle mapStyle) {
		this.mapStyle = mapStyle;
	}
	
	public MapStyle getMapStyle() {
		return this.mapStyle;
	}
	
	public Coordinate getPosition() {
		return this.position;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		
		if (object == null) {
			return false;
		}
		
		if (getClass() != object.getClass()) {
			return false;
		}
			
		Asteroid other = (Asteroid) object;
		
		if (position == null) {
			if (other.position != null) {
				return false;
			}
		} else if (!position.equals(other.position)) {
			return false;
		}
			
		return true;
	}

	
}
