package year2019.day10;

public enum MapStyle {
	IDLE,
	ACTIVE,
	SAME_ANGLE,
	INVISIBLE,
	TESTING
}
