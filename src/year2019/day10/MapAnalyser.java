package year2019.day10;

import java.util.ArrayList;
import java.util.HashMap;

public class MapAnalyser {
	private Map map;
	
	public MapAnalyser(Map map) {
		this.map = map;
	}

	public int calculateMaximumNumberOfAsteroids() {
		int numberOfAstroids = 0;
		
		for (HashMap.Entry<Coordinate, Asteroid> entry : this.map.getMap().entrySet()) {
			int numberOfAsteroidsForAsteroid = this.calculateNumberOfAsteroidsInSight(entry.getValue());
			
			if (numberOfAsteroidsForAsteroid > numberOfAstroids) {
				numberOfAstroids = numberOfAsteroidsForAsteroid;
			}
		}
		
		return numberOfAstroids;
	}
	
	private int calculateNumberOfAsteroidsInSight(Asteroid sourceAsteroid) {
		this.map.resetMapToIdle();
		
		sourceAsteroid.setMapStyle(MapStyle.ACTIVE);
		
		int numberOfAsteroidsInSight = 0;
		
		for (HashMap.Entry<Coordinate, Asteroid> entry : this.map.getMap().entrySet()) {
			Asteroid testAsteroid = entry.getValue();
			
			if (sourceAsteroid.equals(testAsteroid)) {
				continue;
			}

			if (this.hasClearLineOfSight(sourceAsteroid, testAsteroid)) {
				numberOfAsteroidsInSight++;
			}
		}
		
		return numberOfAsteroidsInSight;
	}
	
	private boolean hasClearLineOfSight(Asteroid sourceAsteroid, Asteroid targetAsteroid) {
		targetAsteroid.setMapStyle(MapStyle.TESTING);
		
		int xDelta = sourceAsteroid.getPosition().getX() - targetAsteroid.getPosition().getX();
		int yDelta = sourceAsteroid.getPosition().getY() - targetAsteroid.getPosition().getY();
		
		if (xDelta == 0) {
			// same vertical
		} else if (yDelta == 0) {
			// same horizontal
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		targetAsteroid.setMapStyle(MapStyle.IDLE);
		
		return false;
	}
	
//	private boolean hasClearLineOfSight(Asteroid sourceAsteroid, Asteroid targetAsteroid) {
//		this.map.resetMapToIdle();
//		
//		sourceAsteroid.setMapStyle(MapStyle.ACTIVE);
//		// calculate angle 
//		double angle = this.calculateAngle(sourceAsteroid, targetAsteroid);
//		
//		ArrayList<Asteroid> sameAngleList = new ArrayList<Asteroid>();
//		
//		for (HashMap.Entry<Coordinate, Asteroid> entry : this.map.getMap().entrySet()) {
//			Asteroid testAsteroid = entry.getValue();
//			
//			if (sourceAsteroid.equals(testAsteroid)) {
//				continue;
//			}
//			
//			if (this.calculateAngle(sourceAsteroid, testAsteroid) == angle) {
//				testAsteroid.setMapStyle(MapStyle.SAME_ANGLE);
//				sameAngleList.add(testAsteroid);
//			}
//		}
//		
//		
//		return false;
//	}
//	
//	private double calculateAngle(Asteroid sourceAsteroid, Asteroid targetAsteroid) {
//		// Tangent: tan(0) = Opposite / Adjacent
//		int adjacent = sourceAsteroid.getPosition().getX() - targetAsteroid.getPosition().getX();
//		int opposite = sourceAsteroid.getPosition().getY() - targetAsteroid.getPosition().getY();
//		
//		if (adjacent == 0) {
//			return 999;
//		}
//		
//		double tangent = opposite / adjacent;
//		
//		return tangent;
//	}
	
//	private double calculateDistance(Asteroid sourceAsteroid, Asteroid targetAsteroid) {
//		
//	}

}
