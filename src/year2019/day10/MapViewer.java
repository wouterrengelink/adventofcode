package year2019.day10;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.Timer;

public class MapViewer extends JFrame implements ActionListener {
	private Map map;
	private Timer updateTimer;
	
	private static int xOffSet = 500;
	private static int yOffSet = 500;
	
	public MapViewer(Map map) {
		this.setSize(1200, 1000);
        this.setTitle("Advent of Code - Viewer");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        this.map = map;
        
        this.updateTimer = new Timer(10, this);
        this.updateTimer.start();
	}
	
	@Override
    public void paint(Graphics g) 
    {	
		HashMap<Coordinate, Asteroid> bufferMap = new HashMap<Coordinate, Asteroid>(this.map.getMap());
		
		for(HashMap.Entry<Coordinate, Asteroid> entry : bufferMap.entrySet()) {
			Coordinate coordinate = entry.getKey();
			Asteroid asteroid = entry.getValue();
			
			switch (asteroid.getMapStyle()) {
				case IDLE:
					g.setColor(Color.BLACK);
					break;
				case ACTIVE:
					g.setColor(Color.GREEN);
					break;
				case SAME_ANGLE:
					g.setColor(Color.RED);
					break;
				case INVISIBLE:
					g.setColor(Color.BLUE);
					break;
				case TESTING:
					g.setColor(Color.PINK);
					break;
			}
		
			g.fillOval(coordinate.getX() * 20 + MapViewer.xOffSet, coordinate.getY() * 20 + MapViewer.yOffSet, 20, 20);
		}
    }
	
	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == this.updateTimer) {
			this.repaint();
		}
	}
}
