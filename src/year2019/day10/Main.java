package year2019.day10;

import java.util.ArrayList;
import java.util.HashMap;

import general.InputReader;

public class Main {

	private static String inputFile = "./src/year2019/day10/testInput.txt";
	
	public static void main(String[] args) {
		System.out.println("Booting up day 10 2019");
		
		InputReader inputReader = new InputReader(inputFile);
		ArrayList<String> mapList = inputReader.getInputFromList();
		
		MapScanner mapScanner = new MapScanner(mapList); 
		
		Map map = mapScanner.getAsteroidMap();
		
		MapViewer mapViewer = new MapViewer(map);
		
		MapAnalyser mapAnalyser = new MapAnalyser(map);

		System.out.println("Maximum number of asteroids " + mapAnalyser.calculateMaximumNumberOfAsteroids());
		System.out.println("Done");
	}

}
