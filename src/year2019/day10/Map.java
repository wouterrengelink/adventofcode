package year2019.day10;

import java.util.HashMap;

public class Map {
	private HashMap<Coordinate, Asteroid> map;
	
	public Map(HashMap<Coordinate, Asteroid> map) {
		this.map = map;
	}
	
	public HashMap<Coordinate, Asteroid> getMap() {
		return this.map;
	}
	
	public void resetMapToIdle() {
		for(HashMap.Entry<Coordinate, Asteroid> entry : this.map.entrySet()) {
			Asteroid asteroid = entry.getValue();
			
			asteroid.setMapStyle(MapStyle.IDLE);
		}
	}
}
